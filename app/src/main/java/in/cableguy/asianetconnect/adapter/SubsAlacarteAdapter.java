package in.cableguy.asianetconnect.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.helper.Constants;
import in.cableguy.asianetconnect.helper.Values;
import in.cableguy.asianetconnect.model.Alacarte;

/**
 * Created by rajme on 8/19/2017.
 */

public class SubsAlacarteAdapter extends RecyclerView.Adapter<SubsAlacarteAdapter.ViewHolder> implements Filterable {

    List<Alacarte> filteredData;
    List<Alacarte> alacarteList;
    Map<String,String> alacarteListMap;
    private Activity activity;

    public SubsAlacarteAdapter(List<Alacarte> alacarteList, Map<String,String> alacarteListMap, Activity activity) {
        this.alacarteList = alacarteList;
        this.alacarteListMap =alacarteListMap;
        this.activity = activity;
        this.filteredData= alacarteList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View alacarteView = inflater.inflate(R.layout.subalacartelayout, parent, false);

        ViewHolder viewHolder = new ViewHolder(alacarteView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {
        final TextView alacarteName = viewholder.alacarteNameTV, actDate = viewholder.actDateTV, validDate = viewholder.validDateTV;
        ImageView cancelButton = viewholder.cancelButton;
        final Alacarte alacarte = alacarteList.get(position);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date strDate = sdf.parse(alacarte.getEnd_date());
            if(new Date().after(strDate)){
                cancelButton.setVisibility(View.GONE);
            }else{
                cancelButton.setVisibility(View.VISIBLE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        alacarteName.setText(alacarte.getChannel_name());
        actDate.setText(alacarte.getStart_date());
        validDate.setText(alacarte.getEnd_date());
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                dialog.setMessage("Are you sure you want to cancel "+alacarte.getChannel_name()+" ?");
                dialog.setTitle("Cancel A-La Carte");
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new CancelAlaCarte().execute(alacarte.getStart_date(),alacarte.getEnd_date(),alacarte.getStb_code(),alacarte.getChannel_id()+"", Values.SUBSCRIBER.getSub_id()+"",alacarte.getChannel_service_sch_id()+"");
                    }
                });
                dialog.show();
            }
        });
    }

    private Context getContext(){return activity;}


    @Override
    public int getItemCount() {
        return alacarteList.size();
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        TextView alacarteNameTV, actDateTV, validDateTV;
        ImageView cancelButton;
        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            alacarteNameTV = (TextView)itemView.findViewById(R.id.alacarteNameTV);
            actDateTV = (TextView)itemView.findViewById(R.id.actDateTV);
            validDateTV = (TextView)itemView.findViewById(R.id.validDateTV);
            cancelButton = (ImageView)itemView.findViewById(R.id.cancelButton);
        }
    }


    private class CancelAlaCarte extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            final JSONObject userObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;
            String start_date, end_date,mac_id,channel_id,sub_id,channel_service_sch_id;
            start_date = params[0];
            end_date  = params[1];
            mac_id = params[2];
            channel_id = params[3];
            sub_id = params[4];
            channel_service_sch_id = params[5];
            try {
                url = new URL(Constants.ALA_CARTE_ACTION);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                userObject.put("name",Values.USER_NAME);
                jsonObject.put("start_date",start_date);
                jsonObject.put("end_date",end_date);
                jsonObject.put("mac_id",mac_id);
                jsonObject.put("channel_id",channel_id);
                jsonObject.put("sub_id",sub_id);
                jsonObject.put("channel_service_sch_id",channel_service_sch_id);
                jsonObject.put("user",userObject);
                jsonObject.put("token",Values.TOKEN);
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                conn.connect();
                Log.e("Connection ",conn.toString());
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
/*                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");*/
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray channelArray;
            JSONObject lcoPriceInfo;

            try {
                msg = json.getString("msg");
                status = json.getBoolean("success");
                if(status){
                    Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                    activity.finish();
                    activity.startActivity(activity.getIntent());
                }else{
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
}
