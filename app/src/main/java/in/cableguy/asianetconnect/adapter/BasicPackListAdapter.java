package in.cableguy.asianetconnect.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.helper.Constants;
import in.cableguy.asianetconnect.helper.Values;
import in.cableguy.asianetconnect.model.BasicPack;
import in.cableguy.asianetconnect.ui.activity.SuccessActivity;

/**
 * Created by Raj on 31/08/2017.
 */

public class BasicPackListAdapter extends RecyclerView.Adapter<BasicPackListAdapter.ViewHolder> implements Filterable {
    List<BasicPack> filteredData;
    List<BasicPack> basicPackList;
    Map<String,BasicPack> basicPackMap;
    private Activity activity;
    private ItemFilter mFilter = new ItemFilter();
    List<String> basicPackNames;

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<String> list = basicPackNames;

            int count = list.size();
            final ArrayList<BasicPack> nlist = new ArrayList<>(count);

            String filterableString;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i);
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(basicPackMap.get(filterableString));
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<BasicPack>) results.values;
            Log.e("FILTERR", filteredData.toString());
            basicPackList = filteredData;
            notifyDataSetChanged();
        }
    }
    public BasicPackListAdapter(List<String> basicPackNames,List<BasicPack> basicPackList, Map<String,BasicPack> basicPackMap, Activity activity) {
        Values.SELECTED_ALA_IDS.clear();
        this.basicPackNames = basicPackNames;
        this.basicPackList = basicPackList;
        this.basicPackMap =basicPackMap;
        this.activity = activity;
        this.filteredData= basicPackList;
    }

    public int getCount() {
        return filteredData.size();
    }


    public Object getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public BasicPackListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View basicPackView = inflater.inflate(R.layout.basicpacklistitem, parent, false);

        BasicPackListAdapter.ViewHolder viewHolder = new BasicPackListAdapter.ViewHolder(basicPackView);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final BasicPackListAdapter.ViewHolder viewholder, int position) {
        final TextView basicPackNameTV = viewholder.basicPackNameTV,
                basicPackPeriodTV=viewholder.basicPackPeriodTV,
                basicPackRateTV= viewholder.basicRateTV;
        final TableLayout basicPackTL = viewholder.basicPackTL;
        final BasicPack basicPack = basicPackList.get(position);
        basicPackNameTV.setText(basicPack.getBouquet_name());
        basicPackPeriodTV.setText(basicPack.getBouquet_period());
        if(Values.STB_TYPE.equalsIgnoreCase("SD") || Values.STB_CAT.equalsIgnoreCase("MAIN")){
           basicPackRateTV.setText("₹"+basicPack.getBourate_mainstbsd());
        }else if(Values.STB_TYPE.equalsIgnoreCase("HD") || Values.STB_CAT.equalsIgnoreCase("MAIN")){
           basicPackRateTV.setText("₹"+basicPack.getBourate_mainstbhd());
        }else if(Values.STB_TYPE.equalsIgnoreCase("SD") || !Values.STB_CAT.equalsIgnoreCase("MAIN")){
           basicPackRateTV.setText("₹"+basicPack.getBourate_addstbsd());
        }else if(Values.STB_TYPE.equalsIgnoreCase("HD") || !Values.STB_CAT.equalsIgnoreCase("MAIN")){
           basicPackRateTV.setText("₹"+basicPack.getBourate_addstbhd());
        }
        basicPackTL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                dialog.setMessage("Are you sure you want to select "+basicPack.getBouquet_name()+" ?");
                dialog.setTitle("Change Basic Pack");
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new ChangePlan().execute(basicPack.getBouquet_id()+"", Values.SUBSCRIBER.getSub_id()+"");
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return basicPackList.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        TextView basicPackNameTV, basicRateTV, basicPackPeriodTV;
        TableLayout basicPackTL;
        //CheckBox autoRenewCB, selectAlaCB;
        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            basicPackNameTV = (TextView)itemView.findViewById(R.id.basicPackNameTV);
            basicRateTV = (TextView)itemView.findViewById(R.id.basicRateTV);
            basicPackPeriodTV = (TextView)itemView.findViewById(R.id.basicPackPeriodTV);
            basicPackTL = (TableLayout) itemView.findViewById(R.id.basicPackTL);
            /*autoRenewCB = (CheckBox)itemView.findViewById(R.id.autoRenewCB);
            selectAlaCB = (CheckBox)itemView.findViewById(R.id.selectChannelCB);*/
        }
    }


    private class ChangePlan extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            final JSONObject userObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;
            String new_bouquet_id,sub_id;
            new_bouquet_id = params[0];
            sub_id = params[1];
            try {
                url = new URL(Constants.BASIC_PACK_CHANGE);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                userObject.put("name",Values.USER_NAME);
                JSONObject oldBouquetObject = new JSONObject();
                oldBouquetObject.put("bouquet_id",Integer.parseInt(Values.OLD_BOUQUET_ID));
                oldBouquetObject.put("sub_id",Integer.parseInt(sub_id));
                JSONObject newBouquetObject = new JSONObject();
                newBouquetObject.put("bouquet_id",Integer.parseInt(new_bouquet_id));
                jsonObject.put("old_bouquet",oldBouquetObject);
                jsonObject.put("new_bouquet",newBouquetObject);
                jsonObject.put("user",userObject);
                jsonObject.put("token",Values.TOKEN);
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                conn.connect();
                Log.e("Connection ",conn.toString());
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
/*                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");*/
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray channelArray;
            JSONObject lcoPriceInfo;

            try {
                msg = json.getString("msg");
                status = json.getBoolean("success");
                if(status){
                    Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                    activity.finish();
                    Intent i = new Intent(activity, SuccessActivity.class);
                    activity.startActivity(i);
                }else{
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
}
