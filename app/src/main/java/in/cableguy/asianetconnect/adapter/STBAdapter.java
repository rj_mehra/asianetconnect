package in.cableguy.asianetconnect.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.helper.Values;
import in.cableguy.asianetconnect.model.Stb;
import in.cableguy.asianetconnect.ui.activity.STBDetailsActivity;

/**
 * Created by rajme on 8/19/2017.
 */

public class STBAdapter extends RecyclerView.Adapter<STBAdapter.ViewHolder> implements Filterable {
    List<Stb> filteredData;
    List<Stb> stbList;
    Map<String,Stb> stbListMap;
    private Activity activity;
    public STBAdapter(List<Stb> stbList, Map<String,Stb> stbMap, Activity activity) {
        this.stbList = stbList;
        this.stbListMap=stbMap;
        this.activity = activity;
        this.filteredData=stbList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View stbView = inflater.inflate(R.layout.macidlayout, parent, false);

        ViewHolder viewHolder = new ViewHolder(stbView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewholder, final int position) {
        TextView stbMAC = viewholder.stbMACTV, stbStatus = viewholder.stbStatusTV;

        Stb stb = stbList.get(position);
        Log.e("STB Adapter, STB ",stb.getStb_code());
        stbMAC.setText(stb.getStb_code());
        stbStatus.setText(stb.getStb_status());
        if(stb.getStb_status().equalsIgnoreCase("CON")){
            stbStatus.setTextColor(activity.getResources().getColor(R.color.green));
        }
        stbMAC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Values.POSITION= position;
                Intent intent = new Intent(activity, STBDetailsActivity.class);
                activity.startActivity(intent);
            }
        });

    }

    private Context getContext(){
        return activity;
    }

    @Override
    public int getItemCount() {
        return stbList.size();
    }

    @Override
    public Filter getFilter() {
        return null;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        TextView stbMACTV, stbStatusTV;
        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            stbMACTV = (TextView)itemView.findViewById(R.id.stbMACTV);
            stbStatusTV = (TextView)itemView.findViewById(R.id.stbStatusTV);
        }
    }
}
