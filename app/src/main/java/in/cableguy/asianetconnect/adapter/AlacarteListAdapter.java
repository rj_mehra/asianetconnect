package in.cableguy.asianetconnect.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.helper.Values;
import in.cableguy.asianetconnect.model.Alacarte;

/**
 * Created by rajme on 8/20/2017.
 */

public class AlacarteListAdapter extends RecyclerView.Adapter<AlacarteListAdapter.ViewHolder> implements Filterable{
    List<Alacarte> filteredData;
    List<Alacarte> alacarteList;
    Map<String,Alacarte> alacarteListMap;
    private Activity activity;
    private ItemFilter mFilter = new ItemFilter();
    List<String> alacarteNames;

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<String> list = alacarteNames;

            int count = list.size();
            final ArrayList<Alacarte> nlist = new ArrayList<>(count);

            String filterableString;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i);
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(alacarteListMap.get(filterableString));
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Alacarte>) results.values;
            Log.e("FILTERR", filteredData.toString());
            alacarteList = filteredData;
            notifyDataSetChanged();
        }
    }

    public AlacarteListAdapter(List<String> alacarteNames,List<Alacarte> alacarteList, Map<String,Alacarte> alacarteListMap, Activity activity) {
        Values.SELECTED_ALA_IDS.clear();
        this.alacarteNames = alacarteNames;
        this.alacarteList = alacarteList;
        this.alacarteListMap =alacarteListMap;
        this.activity = activity;
        this.filteredData= alacarteList;
    }


    public int getCount() {
        return filteredData.size();
    }


    public Object getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View alacarteView = inflater.inflate(R.layout.alacartelistitem, parent, false);

        ViewHolder viewHolder = new ViewHolder(alacarteView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewholder, int position) {
        final TextView alacarteName = viewholder.alaNameTV, alaPeriodTV=viewholder.alaPeriodTV, alaRateTV= viewholder.alaRateTV;
        final CheckBox autoRenew = viewholder.autoRenewCB;
        final CheckBox selectChannel = viewholder.selectAlaCB;
        final Alacarte alacarte = alacarteList.get(position);
        alacarteName.setText(alacarte.getChannel_name());
        alaPeriodTV.setText(alacarte.getTotal_period());


        selectChannel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Values.SELECTED_ALA_IDS.add(alacarte);
                    if(alacarte.getAuto_renew().equalsIgnoreCase("YES")){
                        autoRenew.setVisibility(View.VISIBLE);
                    }else if(alacarte.getAuto_renew().equalsIgnoreCase("NO")){
                        autoRenew.setVisibility(View.GONE);
                    }
                }else{
                    Values.SELECTED_ALA_IDS.remove(alacarte);
                    autoRenew.setVisibility(View.GONE);
                }
            }
        });

        autoRenew.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    alacarte.setAutoRenewFlag("Y");
                }else{
                    alacarte.setAutoRenewFlag("N");
                }
            }
        });

        if(Values.STB_TYPE.equalsIgnoreCase("SD") || Values.STB_CAT.equalsIgnoreCase("MAIN")){
            alaRateTV.setText("₹"+alacarte.getChanrate_mainstbsd());
        }else if(Values.STB_TYPE.equalsIgnoreCase("HD") || Values.STB_CAT.equalsIgnoreCase("MAIN")){
            alaRateTV.setText("₹"+alacarte.getChanrate_mainstbhd());
        }else if(Values.STB_TYPE.equalsIgnoreCase("SD") || !Values.STB_CAT.equalsIgnoreCase("MAIN")){
            alaRateTV.setText("₹"+alacarte.getChanrate_addstbsd());
        }else if(Values.STB_TYPE.equalsIgnoreCase("HD") || !Values.STB_CAT.equalsIgnoreCase("MAIN")){
            alaRateTV.setText("₹"+alacarte.getChanrate_addstbhd());
        }


    }

    @Override
    public int getItemCount() {
        return alacarteList.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        TextView alaNameTV, alaRateTV, alaPeriodTV;
        CheckBox autoRenewCB, selectAlaCB;
        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            alaNameTV = (TextView)itemView.findViewById(R.id.alaNameTV);
            alaRateTV = (TextView)itemView.findViewById(R.id.alaRateTV);
            alaPeriodTV = (TextView)itemView.findViewById(R.id.alaPeriodTV);
            autoRenewCB = (CheckBox)itemView.findViewById(R.id.autoRenewCB);
            selectAlaCB = (CheckBox)itemView.findViewById(R.id.selectChannelCB);
        }
    }
}
