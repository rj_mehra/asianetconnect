package in.cableguy.asianetconnect.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.helper.Values;

public class MenuActivity extends AppCompatActivity {
    private ImageView subscriberButton,logoutButton, subsServiceIV,stbReturnIV,stbReplaceIV,stbSwapIV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        /*
            set title for activity
         */
        getSupportActionBar().setTitle("Menu");
        /*
            Define and declare interactions on UI
         */
        subscriberButton = (ImageView)findViewById(R.id.subscriberButton);
        logoutButton = (ImageView)findViewById(R.id.logoutButton);
        subsServiceIV = (ImageView)findViewById(R.id.subsServiceIV);
        stbReturnIV = (ImageView)findViewById(R.id.stbReturnIV);
        stbReplaceIV = (ImageView)findViewById(R.id.stbReplaceIV);
        stbSwapIV = (ImageView)findViewById(R.id.stbSwapIV);
        /*
            Set listener for onscreen actions
         */

        subscriberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Values.menuAccessList.contains(Values.SUB_MENU_PACK_MANAGEMENT)){
                    Intent intent = new Intent(MenuActivity.this,SearchActivity.class);
                    startActivity(intent);
                    Values.SELECTED_SUB_MENU = Values.SUB_MENU_PACK_MANAGEMENT;
                }else{
                    Toast.makeText(MenuActivity.this,Values.UNAUTHORIZED_ACCESS,Toast.LENGTH_SHORT).show();
                }
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
            }
        });

        stbReturnIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Values.menuAccessList.contains(Values.SUB_MENU_STB_RETURN)){
                    Values.CUSTOM_SEARCH_TYPE = "SRT";
                    Intent intent = new Intent(MenuActivity.this,CustomSearchActivity.class);
                    startActivity(intent);
                    Values.SELECTED_SUB_MENU = Values.SUB_MENU_STB_RETURN;
                }else{
                    Toast.makeText(MenuActivity.this,Values.UNAUTHORIZED_ACCESS,Toast.LENGTH_SHORT).show();
                }

            }
        });

        stbReplaceIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Values.menuAccessList.contains(Values.SUB_MENU_STB_REPLACE)){
                    Values.CUSTOM_SEARCH_TYPE = "SRP";
                    Intent intent = new Intent(MenuActivity.this,CustomSearchActivity.class);
                    startActivity(intent);
                    Values.SELECTED_SUB_MENU = Values.SUB_MENU_STB_REPLACE;
                }else{
                    Toast.makeText(MenuActivity.this,Values.UNAUTHORIZED_ACCESS,Toast.LENGTH_SHORT).show();
                }
            }
        });
        stbSwapIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Values.menuAccessList.contains(Values.SUB_MENU_STB_SWAP)){
                    Values.CUSTOM_SEARCH_TYPE = "SW";
                    Intent intent = new Intent(MenuActivity.this,CustomSearchActivity.class);
                    startActivity(intent);
                    Values.SELECTED_SUB_MENU = Values.SUB_MENU_STB_SWAP;
                }else{
                    Toast.makeText(MenuActivity.this,Values.UNAUTHORIZED_ACCESS,Toast.LENGTH_SHORT).show();
                }

            }
        });
        subsServiceIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Values.menuAccessList.contains(Values.SUB_MENU_SUBSCRIBER_SERVICE)){
                    Values.CUSTOM_SEARCH_TYPE = "CS";
                    Intent intent = new Intent(MenuActivity.this, CustomSearchActivity.class);
                    startActivity(intent);
                    Values.SELECTED_SUB_MENU = Values.SUB_MENU_SUBSCRIBER_SERVICE;
                }else{
                    Toast.makeText(MenuActivity.this,Values.UNAUTHORIZED_ACCESS,Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
