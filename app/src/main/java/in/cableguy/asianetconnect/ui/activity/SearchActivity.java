package in.cableguy.asianetconnect.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.helper.Constants;
import in.cableguy.asianetconnect.helper.Values;
import in.cableguy.asianetconnect.model.Stb;
import in.cableguy.asianetconnect.model.Subscriber;
import in.cableguy.asianetconnect.model.SubscriberDetails;

public class SearchActivity extends AppCompatActivity {
    Button searchButton;
    EditText searchED;
    Spinner searchSpinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        searchButton = (Button)findViewById(R.id.searchButton);
        searchED = (EditText)findViewById(R.id.searchED);
        searchSpinner = (Spinner)findViewById(R.id.searchSpinner);
        getSupportActionBar().setTitle("Search");
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(searchSpinner.getSelectedItem().toString().equalsIgnoreCase("Select an Option")){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SearchActivity.this, "Please select an option", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else if(searchSpinner.getSelectedItem().toString().equalsIgnoreCase("Subscriber Code")){
                    new SearchSubs().execute(searchED.getText().toString(),"ac_no");
                }else if(searchSpinner.getSelectedItem().toString().equalsIgnoreCase("STB / MAC No")){
                    new SearchSubs().execute(searchED.getText().toString(),"stb_no");
                }else if(searchSpinner.getSelectedItem().toString().equalsIgnoreCase("Mobile No")){
                    new SearchSubs().execute(searchED.getText().toString(),"mob_no");
                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SearchActivity.this, "Some error occurred!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private class SearchSubs extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SearchActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;
            String search_text = params[0];
            String mode = params[1];

            try {
                url = new URL(Constants.CUSTOMER_SEARCH+"?"+mode+"="+search_text+"&token="+ Values.TOKEN);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("Connection ",conn.toString());
//                OutputStream os = conn.getOutputStream();
//                os.write(jsonObject.toString().getBytes("UTF-8"));
//                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
/*                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");*/
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                    SearchActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //dialog.dismiss();
                            //Snackbar.make(snackbarLayout,"Something went wrong, Kindly retry",Snackbar.LENGTH_LONG).show();

                        }
                    });
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONObject data, subscriber,subscriber_details;
            JSONArray stbs;

            try {
                status = json.getBoolean("success");
                if(status){
                    try{
                        data = json.getJSONObject("data");
                        subscriber = data.getJSONObject("subscriber");
                        /* Set Subscriber */
                        Subscriber subscriberModel = new Subscriber();
                        subscriberModel.setSub_name(subscriber.getString("sub_name"));
                        subscriberModel.setSub_code(subscriber.getString("sub_code"));
                        subscriberModel.setSub_id(subscriber.getInt("sub_id"));
                        Values.SUBSCRIBER = subscriberModel;
                        /* Set Subscriber Details*/
                        subscriber_details = data.getJSONObject("subscriber_details");
                        SubscriberDetails subscriberDetails = new SubscriberDetails();
                        subscriberDetails.setAddress1(subscriber_details.getString("address1"));
                        subscriberDetails.setPhone1(subscriber_details.getString("phone1"));
                        subscriberDetails.setEmail(subscriber_details.getString("email"));
                        subscriberDetails.setBalance(subscriber_details.getDouble("balance"));
                        subscriberDetails.setScheme(subscriber_details.getString("scheme"));
                        subscriberDetails.setStatus(subscriber_details.getString("status"));
                        subscriberDetails.setMac_id(subscriber_details.getString("mac_id"));
                        subscriberDetails.setCenter_id(subscriber_details.getString("center_id"));
                        Values.SUBSCRIBER_CENTER_ID = subscriber_details.getString("center_id");
                        subscriberDetails.setAssociate_id(subscriber_details.getString("associate_id"));
                        Values.SUBSCRIBERDETAILS = subscriberDetails;
                        /* Set STBs */
                        stbs = data.getJSONArray("stbs");
                        Stb stb = new Stb();
                        for(int i=0;i<stbs.length();i++){
                            final JSONObject obj = stbs.getJSONObject(i);
                            JSONObject stbObj = obj.getJSONObject("stb");
                            JSONArray basicArray = obj.getJSONArray("basic_plans");

                            stb.setStb_code(stbObj.getString("stb_code"));
                            Values.MAC_ID_LIST.add(stbObj.getString("stb_code"));
                            stb.setSmart_card_no(stbObj.getString("smart_card_no"));
                            stb.setStb_status(stbObj.getString("stb_status"));
                            stb.setStb_cat(stbObj.getString("stb_cat"));
                            ArrayList<Stb> stbArrayList = new ArrayList<Stb>();
                            for(int j=0;j<basicArray.length();j++){
                                final JSONObject basicObj = basicArray.getJSONObject(i);
                                stb.setBouquet_id(basicObj.getString("bouquet_id"));
                                stb.setStart_date(basicObj.getString("start_date"));
                                stb.setEnd_date(basicObj.getString("end_date"));
                                stb.setBourate_mainstbhd(basicObj.getDouble("bourate_addstbhd"));
                                stb.setLco_price(basicObj.getDouble("lco_price"));
                                stb.setBouquet_name(basicObj.getString("bouquet_name"));
                                stb.setBourate_mainstbsd(basicObj.getDouble("bourate_addstbsd"));
                                stb.setDiscount(basicObj.getDouble("discount"));
                                stbArrayList.add(stb);
                            }
                            Values.STB_ARRAY = stbArrayList;
                            Values.STB = stb;

                        }
                        Intent intent = new Intent(SearchActivity.this, SubscriberDetailsActivity.class);
                        startActivity(intent);
                    }catch (Exception ex){
                        Log.e("token exception ", ex.toString());
                    }
                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SearchActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
}
