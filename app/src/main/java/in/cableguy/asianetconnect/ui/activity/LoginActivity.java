package in.cableguy.asianetconnect.ui.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.helper.Constants;
import in.cableguy.asianetconnect.helper.Values;
import in.cableguy.asianetconnect.model.Menu;
import in.cableguy.asianetconnect.model.SubMenu;

public class LoginActivity extends AppCompatActivity {
    EditText passwordED, userNameED;
    TextView versionNumber;
    Button loginButton;
    String username, password,version, imei;
    ProgressDialog progressDialog;
    String currentVersion;
    int verCode;
    PackageInfo packageInfo;
    TelephonyManager telephonyManager;
    final private static int MY_PERMISSIONS_CALL_PHONE = 500;
    final private static int My_PERMIMSSIONS_READ_PHONE = 100;
    final private static int MY_PERMISSIONS_LOCATION = 200;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        callForRead(); //Calls for runtime permissions in order to allow system access for files and device details.
        /*
        Editable text fields
         */
        passwordED = (EditText)findViewById(R.id.passwordED);
        userNameED = (EditText)findViewById(R.id.userNameED);
        /*
        Button Class
         */
        loginButton = (Button)findViewById(R.id.loginButton);

        /*
            Extract values from the fields
         */
        username = userNameED.getText().toString();
        password = passwordED.getText().toString();

        /*
            Read phone/tablet IMEI
         */


        telephonyManager = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);

        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            currentVersion = packageInfo.versionName;
            verCode = packageInfo.versionCode;
            Values.CURRENT_VERSION = currentVersion + "." + verCode;
            //versionNumber.setText(currentVersion + "." + verCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        /*
            Set listener for button click on login
         */

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Login().execute(userNameED.getText().toString(),passwordED.getText().toString());
            }
        });

    }

    private class Login extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;
            String login_id = params[0];
            Values.USER_NAME = params[0];
            String pass = params[1];

            try {
                url = new URL(Constants.LOGIN_API);
                try {
                    jsonObject.put("login_id", login_id);
                    jsonObject.put("password", pass);
                } catch (JSONException je) {
                    Log.e("Json Exception ", je.toString());
                    Log.e("Url",url.toString());
                }
                Log.e("JSON SEND", jsonObject.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                conn.connect();
                Log.e("Connection ",conn.toString());
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                    LoginActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //dialog.dismiss();
                            //Snackbar.make(snackbarLayout,"Something went wrong, Kindly retry",Snackbar.LENGTH_LONG).show();

                        }
                    });
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            try {
                status = json.getBoolean("success");
                if(status){
                    try{
                        token = json.getString("token");
                        Values.TOKEN = token;
                        Log.e("Token ",token);
                        try{
                            JSONArray menuArray = json.getJSONArray("menus");
                            if(menuArray!=null) {
                                for (int i = 0; i < menuArray.length(); i++) {
                                    try{
                                        Menu menu = new Menu();
                                        JSONObject menuObject = menuArray.getJSONObject(i);
                                        Integer id = menuObject.getInt("id");
                                        String name = menuObject.getString("name");
                                        menu.setId(id);
                                        menu.setName(name);
                                        JSONArray subMenuArray = menuObject.getJSONArray("sub_menus");
                                        if (subMenuArray != null) {
                                            ArrayList<SubMenu> subMenus = new ArrayList<>();
                                            for (int j = 0; j < subMenuArray.length(); j++) {
                                                try{
                                                    SubMenu subMenu = new SubMenu();
                                                    JSONObject subMenuObject = subMenuArray.getJSONObject(j);
                                                    id = subMenuObject.getInt("id");
                                                    name = subMenuObject.getString("name");
                                                    String route = subMenuObject.getString("route");
                                                    subMenu.setId(id);
                                                    subMenu.setName(name);
                                                    Values.menuAccessList.add(route);
                                                    subMenu.setRoute(route);
                                                    JSONArray childMenuArray = menuObject.getJSONArray("child_menus");
                                                    if (childMenuArray != null) {
                                                        try{
                                                            ArrayList<SubMenu> childMenus = new ArrayList<>();
                                                            ArrayList<String> childMenuRoutes = new ArrayList<>();
                                                            for (int k = 0; k < childMenuArray.length(); k++) {
                                                                SubMenu childMenu = new SubMenu();
                                                                JSONObject childMenuObject = menuArray.getJSONObject(k);
                                                                id = childMenuObject.getInt("id");
                                                                name = childMenuObject.getString("name");
                                                                route = childMenuObject.getString("route");
                                                                String type = childMenuObject.getString("type");
                                                                childMenu.setId(id);
                                                                childMenu.setName(name);
                                                                childMenu.setRoute(route);
                                                                childMenus.add(childMenu);
                                                                childMenuRoutes.add(route);
                                                                Values.menuAccessList.add(route);
                                                            }
                                                            Values.childMenuNameMap.put(subMenu.getRoute(), childMenuRoutes);
                                                            subMenu.setChildMenus(childMenus);
                                                        }catch(Exception e){
                                                            continue;
                                                        }
                                                    }
                                                    subMenus.add(subMenu);
                                                }catch(Exception e){
                                                    continue;
                                                }

                                            }
                                            menu.setSubMenus(subMenus);
                                        }
                                        Values.menuArrayList.add(menu);
                                        Values.menuNameMap.put(name, menu);
                                    }catch(Exception e){
                                        continue;
                                    }


                                }

                            }
                        }catch(Exception ex){
                            Log.e("token exception ", ex.toString());
                        }

                        Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }catch (Exception ex){
                        Log.e("token exception ", ex.toString());
                    }
                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
        /*
            Read permissions
         */
        public void callForRead() {

            int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
            try {
                if (currentAPIVersion >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(LoginActivity.this,
                            Manifest.permission.READ_PHONE_STATE)
                            != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(LoginActivity.this,
                                    Manifest.permission.WAKE_LOCK)
                                    != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(LoginActivity.this,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(LoginActivity.this,
                                    Manifest.permission.ACCESS_FINE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(LoginActivity.this,
                                    Manifest.permission.READ_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(LoginActivity.this,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(LoginActivity.this,
                                    Manifest.permission.CAMERA)
                                    != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(LoginActivity.this,
                                    Manifest.permission.BLUETOOTH)
                                    != PackageManager.PERMISSION_GRANTED) {
                        Log.e("Permission", "called");
                        ActivityCompat.requestPermissions(LoginActivity.this,
                                new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.WAKE_LOCK,
                                        Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
                                        Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.BLUETOOTH},
                                My_PERMIMSSIONS_READ_PHONE);

                    } else if (ContextCompat.checkSelfPermission(LoginActivity.this,
                            Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(LoginActivity.this,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(LoginActivity.this,
                                    Manifest.permission.ACCESS_FINE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(LoginActivity.this,
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                MY_PERMISSIONS_LOCATION);

                    } else {

                        Values.LOC_PERM_GRANT = true;
                        Values.READ_PERM_GRANT = true;
                    }
                } else {
                    Values.LOC_PERM_GRANT = true;
                    Values.READ_PERM_GRANT = true;
                }
            } catch (Exception ex) {
                Log.e("Permission Exception", ex + " ");
            }
        }

}
