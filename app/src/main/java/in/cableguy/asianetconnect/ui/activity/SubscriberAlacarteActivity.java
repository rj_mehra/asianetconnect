package in.cableguy.asianetconnect.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.adapter.SubsAlacarteAdapter;
import in.cableguy.asianetconnect.helper.Constants;
import in.cableguy.asianetconnect.helper.Values;
import in.cableguy.asianetconnect.model.Alacarte;

public class SubscriberAlacarteActivity extends AppCompatActivity {
    RecyclerView subsAlacarteRV;
    SubsAlacarteAdapter subsAlacarteAdapter;
    Activity activity;
    Map<String,String> alacarteMap=new HashMap<>();
    ArrayList<Alacarte> alacarteList = new ArrayList<>();
    ImageView cancelButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscriber_alacarte);
        subsAlacarteRV = (RecyclerView)findViewById(R.id.subsAlacarteRV);
        cancelButton = (ImageView)findViewById(R.id.cancelButton);
        activity = this;
        getSupportActionBar().setTitle("Subscribed A-la carte");

        new SubsAlacarte().execute();

    }

    @Override
    protected void onResume() {
        super.onResume();
        new SubsAlacarte().execute();
    }

    private class SubsAlacarte extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SubscriberAlacarteActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;

            try {
                url = new URL(Constants.SUBSCRIBED_ALACARTES+Values.SELECTED_MAC_ID+"?&token="+ Values.TOKEN);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("Connection ",conn.toString());
//                OutputStream os = conn.getOutputStream();
//                os.write(jsonObject.toString().getBytes("UTF-8"));
//                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
/*                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");*/
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray channelArray;
            JSONObject lcoPriceInfo;

            try {
                status = json.getBoolean("success");
                if(status){
                    channelArray = json.getJSONArray("chanArray");
                    for(int i = 0; i < channelArray.length();i++){
                        final JSONObject obj = channelArray.getJSONObject(i);
                        Alacarte alacarte = new Alacarte();
                        alacarte.setChannel_id(obj.getInt("channel_id"));
                        alacarte.setChannel_service_sch_id(obj.getInt("channel_service_sch_id"));
                        alacarte.setChannel_name(obj.getString("channel_name"));
                        alacarte.setChannel_type(obj.getString("channel_type"));
                        alacarte.setStart_date(obj.getString("start_date"));
                        alacarte.setEnd_date(obj.getString("end_date"));
                        alacarte.setStb_code(obj.getString("stb_code"));
                        lcoPriceInfo = obj.getJSONObject("lco_price_info");
                        alacarte.setTotal_addstbhd(lcoPriceInfo.getInt("total_addstbhd"));
                        alacarte.setTotal_addstbsd(lcoPriceInfo.getInt("total_addstbsd"));
                        alacarte.setTotal_mainstbhd(lcoPriceInfo.getInt("total_mainstbhd"));
                        alacarte.setTotal_mainstbsd(lcoPriceInfo.getInt("total_mainstbsd"));
                        alacarte.setChanrate_mainstbhd(obj.getInt("chanrate_mainstbhd"));
                        alacarte.setChanrate_mainstbsd(obj.getInt("chanrate_mainstbsd"));
                        alacarte.setChanrate_addstbhd(obj.getInt("chanrate_addstbhd"));
                        alacarte.setChanrate_addstbsd(obj.getInt("chanrate_addstbsd"));
                        alacarteList.add(alacarte);
                    }
                    //Values.ALACARTE = alacarte;
                    subsAlacarteAdapter = new SubsAlacarteAdapter(alacarteList,alacarteMap,activity);
                    subsAlacarteRV.setAdapter(subsAlacarteAdapter);
                    LinearLayoutManager layoutManager
                            = new LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false);
                    subsAlacarteRV.setLayoutManager(layoutManager);

                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SubscriberAlacarteActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
}
