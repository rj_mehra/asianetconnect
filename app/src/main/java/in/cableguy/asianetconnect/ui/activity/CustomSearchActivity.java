package in.cableguy.asianetconnect.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.Toast;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.helper.Values;

public class CustomSearchActivity extends AppCompatActivity {
    private Button submitButton, stbReplaceButton;
    private EditText searchTermED, searchED;
    private TableLayout stbReturnTL,customSearchTL;
    private Spinner customSearchOption;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_search);
        getSupportActionBar().setTitle("Search");
        submitButton = (Button)findViewById(R.id.submitButton);
        searchTermED = (EditText)findViewById(R.id.searchTermED);
        stbReturnTL = (TableLayout)findViewById(R.id.stbReturnTL);
        stbReplaceButton = (Button)findViewById(R.id.stbReplaceButton);
        searchED = (EditText)findViewById(R.id.searchED);
        customSearchTL = (TableLayout)findViewById(R.id.customSearchTL);
        customSearchOption = (Spinner)findViewById(R.id.customSearchOption);
        if(Values.CUSTOM_SEARCH_TYPE.equalsIgnoreCase("SRP") || Values.CUSTOM_SEARCH_TYPE.equalsIgnoreCase("CS")){
            if(stbReturnTL.getVisibility()== View.GONE || customSearchTL.getVisibility()==View.VISIBLE){
                stbReturnTL.setVisibility(View.VISIBLE);
                customSearchTL.setVisibility(View.GONE);
            }
        }else {
            if(stbReturnTL.getVisibility()==View.VISIBLE || customSearchTL.getVisibility()==View.GONE){
                stbReturnTL.setVisibility(View.GONE);
                customSearchTL.setVisibility(View.VISIBLE);
            }

        }
        stbReplaceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Values.SEARCH_TERM = searchED.getText().toString();
                if(searchED.getText().toString().trim().equalsIgnoreCase("")){
                    Toast.makeText(CustomSearchActivity.this, "Please enter a search term", Toast.LENGTH_SHORT).show();
                }else{
                    if(Values.CUSTOM_SEARCH_TYPE.equalsIgnoreCase("SRP")){
                        Intent intent = new Intent(CustomSearchActivity.this,StbReplaceActivity.class);
                        startActivity(intent);
                    }else if(Values.CUSTOM_SEARCH_TYPE.equalsIgnoreCase("CS")){
                        Intent intent = new Intent(CustomSearchActivity.this,SubscriberServiceActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(CustomSearchActivity.this, "Please go back to previous screen and select again!", Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Values.SEARCH_TERM = searchTermED.getText().toString();
                if(searchTermED.getText().toString().trim().equalsIgnoreCase("")){
                    Toast.makeText(CustomSearchActivity.this, "Please enter a search term", Toast.LENGTH_SHORT).show();
                }else{
                    if(Values.CUSTOM_SEARCH_TYPE.equalsIgnoreCase("SRT")){
                        Log.e("I was here in ", " SRT");
                        Intent intent = new Intent(CustomSearchActivity.this,STBReturnActivity.class);
                        startActivity(intent);
                    }else if(Values.CUSTOM_SEARCH_TYPE.equalsIgnoreCase("SW")){
                        Log.e("I was here in ", " SW");
                        Intent intent = new Intent(CustomSearchActivity.this,StbSwapActivity.class);
                        startActivity(intent);
                    }
                }


            }
        });
        customSearchOption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String item = (String)adapterView.getItemAtPosition(position);
                if(item.equalsIgnoreCase("Subscriber Code")){
                    Values.SEARCH_PARAM = "SUB_CODE";
                }else{
                    Values.SEARCH_PARAM = "STB_CODE";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }



}
