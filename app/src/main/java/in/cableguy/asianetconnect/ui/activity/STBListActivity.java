package in.cableguy.asianetconnect.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.HashMap;
import java.util.Map;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.adapter.STBAdapter;
import in.cableguy.asianetconnect.helper.Values;
import in.cableguy.asianetconnect.model.Stb;

public class STBListActivity extends AppCompatActivity {
    RecyclerView stbListRV;
    STBAdapter stbAdapter;
    Activity activity;
    Map<String,Stb> stbMap=new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stblist);
        stbListRV = (RecyclerView)findViewById(R.id.stbListRV);
        activity = this;
        getSupportActionBar().setTitle("List of MACs");
        stbAdapter = new STBAdapter(Values.STB_ARRAY,stbMap,activity);
        stbListRV.setAdapter(stbAdapter);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false);
        stbListRV.setLayoutManager(layoutManager);
    }

}
