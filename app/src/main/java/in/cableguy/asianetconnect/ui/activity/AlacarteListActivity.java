package in.cableguy.asianetconnect.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.adapter.AlacarteListAdapter;
import in.cableguy.asianetconnect.helper.Constants;
import in.cableguy.asianetconnect.helper.Values;
import in.cableguy.asianetconnect.model.Alacarte;

/**
 * @author Raj
 * The type Alacarte list activity.
 */
public class AlacarteListActivity extends AppCompatActivity {
    /**
     * The Ala carte list rv.
     */
    RecyclerView alaCarteListRV;
    /**
     * The Alacarte list adapter.
     */
    AlacarteListAdapter alacarteListAdapter;
    /**
     * The Activity.
     */
    Activity activity;
    /**
     * The Alacarte name map.
     */
    Map<String,Alacarte> alacarteNameMap=new HashMap<>();
    /**
     * The Alacarte name list.
     */
    ArrayList<Alacarte> alacarteNameList = new ArrayList<>();
    /**
     * The Duration option spinner.
     */
    Spinner durationOptionSpinner, /**
     * The Genre spinner.
     */
    genreSpinner, /**
     * The Scheme spinner.
     */
    schemeSpinner;
    /**
     * The Search bar.
     */
    EditText searchBar;
    /**
     * The Alacarte names.
     */
    ArrayList<String> alacarteNames = new ArrayList<>();
    /**
     * The Genre list names.
     */
    ArrayList<String> genreListNames = new ArrayList<>();
    /**
     * The Genre list ids.
     */
    ArrayList<Integer> genreListIds = new ArrayList<>();
    /**
     * The Service list names.
     */
    ArrayList<String> serviceListNames = new ArrayList<>();
    /**
     * The Service list ids.
     */
    ArrayList<Integer> serviceListIds = new ArrayList<>();

    /**
     * The Genre adapter.
     */
    ArrayAdapter<String> genreAdapter;
    /**
     * The Services adapter.
     */
    ArrayAdapter<String > servicesAdapter;
    /**
     * The Auto renew cb.
     */
    CheckBox autoRenewCB, /**
     * The Select channel cb.
     */
    selectChannelCB;
    /**
     * The Submit button.
     */
    Button submitButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alacarte_list);
        getSupportActionBar().setTitle("A-la Carte list");
        durationOptionSpinner = (Spinner)findViewById(R.id.durationOptionSpinner);
        genreSpinner = (Spinner)findViewById(R.id.genreSpinner);
        schemeSpinner = (Spinner)findViewById(R.id.schemeSpinner);
        alaCarteListRV = (RecyclerView)findViewById(R.id.alaCarteListRV);
        activity = this;
        selectChannelCB = (CheckBox)findViewById(R.id.selectChannelCB);
        autoRenewCB = (CheckBox)findViewById(R.id.autoRenewCB);
        searchBar = (EditText)findViewById(R.id.searchBar);
        String option = durationOptionSpinner.getSelectedItem().toString();
        genreAdapter = new ArrayAdapter<String>(AlacarteListActivity.this, android.R.layout.simple_spinner_item, android.R.id.text1);
        genreAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genreSpinner.setAdapter(genreAdapter);
        servicesAdapter = new ArrayAdapter<String>(AlacarteListActivity.this, android.R.layout.simple_spinner_item, android.R.id.text1);
        servicesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        schemeSpinner.setAdapter(servicesAdapter);
        submitButton = (Button)findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AddAlaCarte().execute();
            }
        });
        new FetchGenre().execute();
        durationOptionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Values.DURATION = parent.getSelectedItem().toString();
                switch (parent.getSelectedItem().toString()){
                    case "1 month":
                            Values.DURATION = "30";
                            break;
                    case "3 months":
                        Values.DURATION = "90";
                            break;
                    case "6 months":
                        Values.DURATION = "180";
                            break;
                    case "12 months":
                        Values.DURATION = "360";
                            break;
                    case "Others":
                        Values.DURATION = "Others";
                            break;
                }
                new AlaCarteList().execute(parent.getSelectedItem().toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        genreSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try{
                    Values.GENRE_ID = String.valueOf(genreListIds.get(position));
                    new FetchServices().execute(String.valueOf(genreListIds.get(position)));

                }catch (Exception ex){
                    Log.e("Genrespinner error ",ex.toString());
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        schemeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try{
                    Values.SERVICE_ID = String.valueOf(serviceListIds.get(position));
                    new AlaCarteList().execute(Values.DURATION,Values.GENRE_ID,Values.SERVICE_ID);

                }catch (Exception ex){
                    Log.e("Service Spinner error",ex.toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        new AlaCarteList().execute(option);
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                AlacarteListActivity.this.alacarteListAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     *
     * Background task for calling list of available alacartes
     */
    private class AlaCarteList extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Json.
         */
        String json = "";
        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();
        /**
         * The Progress dialog.
         */
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AlacarteListActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;
            String duration = params[0];

            try {
                alacarteNameList.clear();
                alacarteNameMap.clear();
                alacarteNames.clear();
                url = new URL(Constants.ALACARTE_LIST+"?duration="+duration+"&cat=ALA"+"&sub_id="+Values.SUBSCRIBER_ID+"&token="+ Values.TOKEN+"&service_id="+Values.SERVICE_ID+"&genre_id="+Values.GENRE_ID);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("Connection ",conn.toString());
//                OutputStream os = conn.getOutputStream();
//                os.write(jsonObject.toString().getBytes("UTF-8"));
//                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
/*                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");*/
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray channelArray;
            JSONObject lcoPriceInfo;

            try {
                status = json.getBoolean("success");
                if(status){
                    alacarteNameList.clear();
                    channelArray = json.getJSONArray("chanArray");
                    for(int i = 0; i < channelArray.length();i++){
                        final JSONObject obj = channelArray.getJSONObject(i);
                        Alacarte alacarte = new Alacarte();
                        alacarte.setChannel_id(obj.getInt("channel_id"));
                        alacarte.setChannel_name(obj.getString("channel_name"));
                        alacarte.setChannel_type(obj.getString("channel_type"));
                        alacarte.setTotal_period(obj.getString("channel_period").trim());
                        lcoPriceInfo = obj.getJSONObject("lco_price_info");
                        alacarte.setTotal_addstbhd(lcoPriceInfo.getInt("total_addstbhd"));
                        alacarte.setTotal_addstbsd(lcoPriceInfo.getInt("total_addstbsd"));
                        alacarte.setTotal_mainstbhd(lcoPriceInfo.getInt("total_mainstbhd"));
                        alacarte.setTotal_mainstbsd(lcoPriceInfo.getInt("total_mainstbsd"));
                        alacarte.setChanrate_mainstbhd(obj.getInt("chanrate_mainstbhd"));
                        alacarte.setChanrate_mainstbsd(obj.getInt("chanrate_mainstbsd"));
                        alacarte.setChanrate_addstbhd(obj.getInt("chanrate_addstbhd"));
                        alacarte.setChanrate_addstbsd(obj.getInt("chanrate_addstbsd"));
                        alacarte.setAuto_renew(obj.getString("auto_renew"));
                        alacarte.setNo_of_months(obj.getInt("no_of_months"));
                        alacarte.setChannel_service_sch_id(obj.getInt("channel_service_sch_id"));
                        alacarteNameList.add(alacarte);
                        alacarteNames.add(alacarte.getChannel_name());
                        alacarteNameMap.put(alacarte.getChannel_name(),alacarte);
                    }
                    //Values.ALACARTE = alacarte;
                    alacarteListAdapter = new AlacarteListAdapter(alacarteNames,alacarteNameList,alacarteNameMap,activity);
                    alaCarteListRV.setAdapter(alacarteListAdapter);
                    LinearLayoutManager layoutManager
                            = new LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false);
                    alaCarteListRV.setLayoutManager(layoutManager);

                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(AlacarteListActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }

    /**
     * Background task for calling list of Genres
     */


    private class FetchGenre extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Json.
         */
        String json = "";
        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();
        /**
         * The Progress dialog.
         */
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AlacarteListActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;

            try {
                url = new URL(Constants.CHANNEL_GENRES+Values.SUBSCRIBER_CENTER_ID+"?token="+ Values.TOKEN);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("Connection ",conn.toString());
//                OutputStream os = conn.getOutputStream();
//                os.write(jsonObject.toString().getBytes("UTF-8"));
//                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
/*                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");*/
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray genreArray;
            JSONObject lcoPriceInfo;
            try {
                status = json.getBoolean("success");

                if(status){
                    alacarteNameList.clear();
                    genreArray = json.getJSONArray("genres");
                    for(int i = 0; i < genreArray.length();i++){
                        final JSONObject obj = genreArray.getJSONObject(i);
                        genreListNames.add(obj.getString("genre_name"));
                        genreListIds.add(obj.getInt("genre_id"));
                        genreAdapter.add(obj.getString("genre_name"));
                    }
                    new FetchServices().execute(String.valueOf(genreListIds.get(0)));
                    genreAdapter.notifyDataSetChanged();
                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(AlacarteListActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }


    /**
     *
     * Background task for calling list of services
     */


    private class FetchServices extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Json.
         */
        String json = "";
        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();
        /**
         * The Progress dialog.
         */
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AlacarteListActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;
            String genreId = params[0];

            try {
                serviceListIds.clear();
                serviceListNames.clear();
                servicesAdapter.clear();
                url = new URL(Constants.FETCH_SERVICES+Values.SUBSCRIBER_CENTER_ID+"?token="+ Values.TOKEN+"&genre_id="+genreId);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("Connection ",conn.toString());
//                OutputStream os = conn.getOutputStream();
//                os.write(jsonObject.toString().getBytes("UTF-8"));
//                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
/*                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");*/
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray genreArray;
            JSONObject lcoPriceInfo;
            try {
                status = json.getBoolean("success");

                if(status){
                    alacarteNameList.clear();
                    genreArray = json.getJSONArray("services");
                    for(int i = 0; i < genreArray.length();i++){
                        final JSONObject obj = genreArray.getJSONObject(i);
                        serviceListNames.add(obj.getString("service_name"));
                        serviceListIds.add(obj.getInt("service_id"));
                        servicesAdapter.add(obj.getString("service_name"));
                    }
                    genreAdapter.notifyDataSetChanged();
                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(AlacarteListActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }


    /**
     * Background task for adding selected alacarte in form of array to subscriber account
     */


    private class AddAlaCarte extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Json.
         */
        String json = "";
        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();
        /**
         * The Progress dialog.
         */
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            final JSONObject userObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;
            try {
                url = new URL(Constants.ADD_ALACARTE+Values.SUBSCRIBER_ID);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                userObject.put("name",Values.USER_NAME);
                Calendar c = Calendar.getInstance();
                System.out.println("Current time => "+c.getTime());
                JSONArray alacartes = new JSONArray();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String formattedDate = df.format(c.getTime());
                for(int i=0;i<Values.SELECTED_ALA_IDS.size();i++){
                    Alacarte alacarte = Values.SELECTED_ALA_IDS.get(i);
                    JSONObject object= new JSONObject();
                    object.put("start_date",formattedDate);
                    Integer noOfMonths = alacarte.getNo_of_months();
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.MONTH, noOfMonths);
                    String formattedEndDate = df.format(cal.getTime());
                    object.put("end_date",formattedEndDate);
                    object.put("channel_id",alacarte.getChannel_id());
                    object.put("channel_service_sch_id",alacarte.getChannel_service_sch_id());
                    object.put("mac_id",Values.SELECTED_MAC_ID);
                    alacartes.put(i,object);
                }
                jsonObject.put("sub_id",Values.SUBSCRIBER_ID);
                jsonObject.put("user",userObject);
                jsonObject.put("token",Values.TOKEN);
                jsonObject.put("alacartes",alacartes);
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                conn.connect();
                Log.e("Connection ",conn.toString());
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
/*                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");*/
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;

            try {
                msg = json.getString("msg");
                status = json.getBoolean("success");
                if(status){
                    Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(AlacarteListActivity.this,SuccessActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }else{
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
}
