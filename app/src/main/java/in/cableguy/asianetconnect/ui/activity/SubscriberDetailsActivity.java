package in.cableguy.asianetconnect.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.helper.Values;

public class SubscriberDetailsActivity extends AppCompatActivity {
    TextView subscriberNameTV,subscriberCodeTV,subscriberBalanceTV,mobileTV,emailTV,statusTV,subscriberAddressTV,planDetailsTV;
    Button stbDetailsButton;

    //alaRateTV.setText("₹"+alacarte.getChanrate_mainstbsd());
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscriber_details);
        getSupportActionBar().setTitle("Subscriber Details");
        subscriberNameTV = (TextView)findViewById(R.id.subscriberNameTV);
        subscriberCodeTV = (TextView)findViewById(R.id.subscriberCodeTV);
        subscriberBalanceTV=(TextView)findViewById(R.id.subscriberBalanceTV);
        mobileTV = (TextView)findViewById(R.id.mobileTV);
        emailTV = (TextView)findViewById(R.id.emailTV);
        statusTV = (TextView)findViewById(R.id.statusTV);
        subscriberAddressTV = (TextView)findViewById(R.id.subscriberAddressTV);
        planDetailsTV = (TextView)findViewById(R.id.planDetailsTV);
        stbDetailsButton = (Button)findViewById(R.id.stbDetailsButton);
        subscriberNameTV.setText(Values.SUBSCRIBER.getSub_name());
        subscriberCodeTV.setText(Values.SUBSCRIBER.getSub_code());
        subscriberBalanceTV.setText(Values.SUBSCRIBERDETAILS.getBalance()+"");
        Values.SUBSCRIBER_ID = Values.SUBSCRIBER.getSub_id();
        Values.SUBSCRIBER_CODE = Values.SUBSCRIBER.getSub_code();
        mobileTV.setText(Values.SUBSCRIBERDETAILS.getPhone1());
        emailTV.setText(Values.SUBSCRIBERDETAILS.getEmail());
        statusTV.setText(Values.SUBSCRIBERDETAILS.getStatus());
        if(Values.SUBSCRIBERDETAILS.getStatus().equalsIgnoreCase("Active")){
            statusTV.setTextColor(getResources().getColor(R.color.green));
        }else{
            statusTV.setTextColor(getResources().getColor(R.color.red));
        }
        subscriberAddressTV.setText(Values.SUBSCRIBERDETAILS.getAddress1());
        planDetailsTV.setText(Values.SUBSCRIBERDETAILS.getScheme());

        stbDetailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubscriberDetailsActivity.this,STBListActivity.class);
                startActivity(intent);
            }
        });
    }
}
