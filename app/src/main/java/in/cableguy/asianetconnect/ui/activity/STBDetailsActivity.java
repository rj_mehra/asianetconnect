package in.cableguy.asianetconnect.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.helper.Constants;
import in.cableguy.asianetconnect.helper.Values;
import in.cableguy.asianetconnect.model.Stb;

public class STBDetailsActivity extends AppCompatActivity {
    TextView stbcodeTV, stbStatusTV, stbCatTV,bouquetIdTV, bouquetNameTV,startDateTV, endDateTV,basicPlanChangeTV;
    Button viewAlacartesButton, addAlacarteButton, activateButton, deactivateButton, retrackButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stbdetails);
        getSupportActionBar().setTitle("MAC Details");
        stbcodeTV = (TextView)findViewById(R.id.stbcodeTV);
        stbStatusTV = (TextView)findViewById(R.id.stbStatusTV);
        stbCatTV = (TextView)findViewById(R.id.stbCatTV);
        bouquetIdTV = (TextView)findViewById(R.id.bouquetIdTV);
        bouquetNameTV = (TextView)findViewById(R.id.bouquetNameTV);
        startDateTV = (TextView)findViewById(R.id.startDateTV);
        endDateTV = (TextView)findViewById(R.id.endDateTV);
        basicPlanChangeTV = (TextView)findViewById(R.id.basicPlanChangeTV);
        viewAlacartesButton = (Button)findViewById(R.id.viewAlacartesButton);
        addAlacarteButton = (Button)findViewById(R.id.addAlacarteButton);
        activateButton = (Button)findViewById(R.id.activateButton);
        deactivateButton = (Button)findViewById(R.id.deactivateButton);
        retrackButton = (Button)findViewById(R.id.retrackButton);
        final Stb stb= Values.STB_ARRAY.get(Values.POSITION);
        stbcodeTV.setText(stb.getStb_code());
        Values.SELECTED_MAC_ID = stb.getStb_code();
        stbStatusTV.setText(stb.getStb_status());
        stbCatTV.setText(stb.getStb_cat());
        Values.STB_TYPE = stb.getStb_cat();
        Values.STB_CAT = stb.getStb_cat();
        bouquetIdTV.setText(stb.getBouquet_id());
        bouquetNameTV.setText(stb.getBouquet_name());
        startDateTV.setText(stb.getStart_date());
        endDateTV.setText(stb.getEnd_date());
        basicPlanChangeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(STBDetailsActivity.this, BasicPackListActivity.class);
                Values.OLD_BOUQUET_ID = stb.getBouquet_id();
                startActivity(intent);
            }
        });
        viewAlacartesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(STBDetailsActivity.this, SubscriberAlacarteActivity.class);
                startActivity(intent);
            }
        });
        addAlacarteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(STBDetailsActivity.this, AlacarteListActivity.class);
                startActivity(intent);
            }
        });
        activateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SubsAlacarte().execute("2");
            }
        });
        deactivateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SubsAlacarte().execute("3");
            }
        });
        retrackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SubsAlacarte().execute("1");
            }
        });
    }


    private class SubsAlacarte extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(STBDetailsActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            final JSONObject userObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;
            int action = Integer.parseInt(params[0]);
            try {
                url = new URL(Constants.STB_ACTION);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                jsonObject.put("sub_id",Values.SUBSCRIBER_ID);
                jsonObject.put("sub_code", Values.SUBSCRIBER_CODE);
                jsonObject.put("mac_id",Values.SELECTED_MAC_ID);
                jsonObject.put("action", action);
                userObject.put("name",Values.USER_NAME);
                jsonObject.put("user",userObject);
                jsonObject.put("token",Values.TOKEN);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                conn.connect();
                Log.e("JSONObject", jsonObject.toString());
                Log.e("Connection ",conn.toString());
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
/*                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");*/
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray channelArray;
            JSONObject lcoPriceInfo;

            try {
                status = json.getBoolean("success");
                msg = json.getString("msg");
                if(status){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(STBDetailsActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(STBDetailsActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
}
