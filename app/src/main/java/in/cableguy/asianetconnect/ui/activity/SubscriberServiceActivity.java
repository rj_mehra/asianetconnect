package in.cableguy.asianetconnect.ui.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.helper.Constants;
import in.cableguy.asianetconnect.helper.ConvertToISOString;
import in.cableguy.asianetconnect.helper.Values;
import in.cableguy.asianetconnect.model.Reasons;
import in.cableguy.asianetconnect.model.Stb;
import in.cableguy.asianetconnect.model.Subscriber;
import in.cableguy.asianetconnect.model.SubscriberDetails;

public class SubscriberServiceActivity extends AppCompatActivity {

    private ArrayList<Reasons> reasonsArrayList = new ArrayList<Reasons>();
    private ArrayList<String> reasonList = new ArrayList<>();
    private ArrayList<Integer> reasonIdList = new ArrayList<>();
    private SearchableSpinner reasonSpinner,macIdSpinner;
    private TextView subsNameTV,subsAddressTV,mobileTV,emailTV,subscriberStatusTV,associateIdTV,subsCodeTV,dateTV,reconnectionDateTV;
    private ArrayList<String> macIdArrayList = new ArrayList<>();
    private TableLayout disconnectTL;
    private Button disconnectButton,reconnectButton,submitButton;
    private TableRow reasonTR,disconDateTR;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscriber_service);
        getSupportActionBar().setTitle("Subscriber Service");
        reasonSpinner = (SearchableSpinner)findViewById(R.id.reasonSpinner);
        macIdSpinner = (SearchableSpinner)findViewById(R.id.macIdSpinner);
        subsNameTV = (TextView)findViewById(R.id.subsNameTV);
        subsAddressTV = (TextView)findViewById(R.id.subsAddressTV);
        mobileTV = (TextView)findViewById(R.id.mobileTV);
        emailTV = (TextView)findViewById(R.id.emailTV);
        subscriberStatusTV = (TextView)findViewById(R.id.subscriberStatusTV);
        associateIdTV = (TextView)findViewById(R.id.associateIdTV);
        subsCodeTV = (TextView)findViewById(R.id.subsCodeTV);
        dateTV = (TextView)findViewById(R.id.dateTV);
        reconnectionDateTV = (TextView)findViewById(R.id.reconnectionDateTV);
        disconnectTL = (TableLayout)findViewById(R.id.disconnectTL);
        disconnectButton = (Button)findViewById(R.id.disconnectButton);
        reconnectButton = (Button)findViewById(R.id.reconnectButton);
        reasonTR = (TableRow)findViewById(R.id.reasonTR);
        disconDateTR = (TableRow)findViewById(R.id.disconDateTR);
        submitButton = (Button)findViewById(R.id.submitButton);
        new FetchReasons().execute();
        new SearchSubs().execute();
        dateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickDate(view);
            }
        });

        reasonSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                final Reasons reasons = reasonsArrayList.get(i);
                Values.REASON_ID = reasons.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        reconnectionDateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickReconDate(view);
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(reconnectionDateTV.getText().toString().equalsIgnoreCase("Pick Date")){
                    Toast.makeText(SubscriberServiceActivity.this, "Please select both the dates", Toast.LENGTH_SHORT).show();
                }else{
                    if(Values.SUBSCRIBER_SERVICE.equalsIgnoreCase("DIS")){
                        new DisconnectRequest().execute(dateTV.getText().toString(),reconnectionDateTV.getText().toString(),macIdSpinner.getSelectedItem().toString());
                    }else if(Values.SUBSCRIBER_SERVICE.equalsIgnoreCase("RECON")){
                        new ReconnectRequest().execute(reconnectionDateTV.getText().toString(),macIdSpinner.getSelectedItem().toString());
                    }
                }

            }
        });

    }



    /**
     * The Selected date.
     */
    String selectedDate;

    /**
     * Pick date method for showing calendar to select date.
     *
     * @param view the view
     */
    public void pickDate(View view) {
        final Calendar c1 = Calendar.getInstance();
        DatePickerDialog datedialog = new DatePickerDialog(SubscriberServiceActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(
                            final DatePicker view,
                            int year,
                            int monthOfYear,
                            int dayOfMonth) {
                        final String date1 = year + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + String.format("%02d", dayOfMonth);
                        AlertDialog.Builder dialog = new AlertDialog.Builder(SubscriberServiceActivity.this);
                        dialog.setTitle("Date Selection");
                        dialog.setMessage("Do you want to select " + date1 + " ?");
                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                            }
                        });
                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                selectedDate = date1;
                                final DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                                Date d = new Date();
                                try {
                                    d = format.parse(date1);
                                } catch (Exception e) {
                                }
                                selectedDate = ConvertToISOString.getISO8601StringForDate(d);
                                SubscriberServiceActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        dateTV.setText(date1);
                                    }
                                });
                            }
                        });
                        dialog.show();
                    }

                }, c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DATE));
        datedialog.getDatePicker().setMinDate(c1.getTimeInMillis());
        datedialog.show();
    }

    public void pickReconDate(View view) {
        final Calendar c1 = Calendar.getInstance();
        DatePickerDialog datedialog = new DatePickerDialog(SubscriberServiceActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(
                            final DatePicker view,
                            int year,
                            int monthOfYear,
                            int dayOfMonth) {
                        final String date1 = year + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + String.format("%02d", dayOfMonth);
                        AlertDialog.Builder dialog = new AlertDialog.Builder(SubscriberServiceActivity.this);
                        dialog.setTitle("Date Selection");
                        dialog.setMessage("Do you want to select " + date1 + " ?");
                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                            }
                        });
                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                selectedDate = date1;
                                final DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                                Date d = new Date();
                                try {
                                    d = format.parse(date1);
                                } catch (Exception e) {
                                }
                                selectedDate = ConvertToISOString.getISO8601StringForDate(d);
                                SubscriberServiceActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        reconnectionDateTV.setText(date1);
                                    }
                                });
                            }
                        });
                        dialog.show();
                    }

                }, c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DATE));
        datedialog.getDatePicker().setMinDate(c1.getTimeInMillis());
        datedialog.show();
    }
    private class SearchSubs extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SubscriberServiceActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;

            try {
                String strURL = Constants.CUSTOMER_SEARCH;
                if(Values.SEARCH_PARAM.equalsIgnoreCase("SUB_CODE")){
                    strURL +="?ac_no="+Values.SEARCH_TERM+"&token="+ Values.TOKEN ;
                }else{
                    strURL +="?stb_no="+Values.SEARCH_TERM+"&token="+ Values.TOKEN ;
                }
                url = new URL(strURL.replaceAll(" ","%20"));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("Connection ",conn.toString());
                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {
                Log.e("JSONEX2 : ", e.toString());
            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONObject data, subscriber,subscriber_details;
            JSONArray stbs;

            try {
                status = json.getBoolean("success");
                if(status){
                    try{
                        data = json.getJSONObject("data");
                        subscriber = data.getJSONObject("subscriber");
                        /* Set Subscriber */
                        Subscriber subscriberModel = new Subscriber();
                        subscriberModel.setSub_name(subscriber.getString("sub_name"));
                        subscriberModel.setSub_code(subscriber.getString("sub_code"));
                        subscriberModel.setSub_id(subscriber.getInt("sub_id"));
                        Values.SUBSCRIBER = subscriberModel;
                        /* Set Subscriber Details*/
                        subscriber_details = data.getJSONObject("subscriber_details");
                        SubscriberDetails subscriberDetails = new SubscriberDetails();
                        subscriberDetails.setAddress1(subscriber_details.getString("address1"));
                        subscriberDetails.setPhone1(subscriber_details.getString("phone1"));
                        subscriberDetails.setEmail(subscriber_details.getString("email"));
                        subscriberDetails.setBalance(subscriber_details.getDouble("balance"));
                        subscriberDetails.setScheme(subscriber_details.getString("scheme"));
                        subscriberDetails.setStatus(subscriber_details.getString("status"));
                        subscriberDetails.setMac_id(subscriber_details.getString("mac_id"));
                        subscriberDetails.setCenter_id(subscriber_details.getString("center_id"));
                        Values.SUBSCRIBER_CENTER_ID = subscriber_details.getString("center_id");
                        subscriberDetails.setAssociate_id(subscriber_details.getString("associate_id"));
                        Values.SUBSCRIBERDETAILS = subscriberDetails;
                        /* Set STBs */
                        stbs = data.getJSONArray("stbs");
                        Stb stb = new Stb();
                        for(int i=0;i<stbs.length();i++){
                            final JSONObject obj = stbs.getJSONObject(i);
                            JSONObject stbObj = obj.getJSONObject("stb");
                            JSONArray basicArray = obj.getJSONArray("basic_plans");

                            stb.setStb_code(stbObj.getString("stb_code"));
                            Values.MAC_ID_LIST.add(stbObj.getString("stb_code"));
                            stb.setSmart_card_no(stbObj.getString("smart_card_no"));
                            stb.setStb_status(stbObj.getString("stb_status"));
                            stb.setStb_cat(stbObj.getString("stb_cat"));
                            ArrayList<Stb> stbArrayList = new ArrayList<Stb>();
                            for(int j=0;j<basicArray.length();j++){
                                final JSONObject basicObj = basicArray.getJSONObject(i);
                                stb.setBouquet_id(basicObj.getString("bouquet_id"));
                                stb.setStart_date(basicObj.getString("start_date"));
                                stb.setEnd_date(basicObj.getString("end_date"));
                                stb.setBourate_mainstbhd(basicObj.getDouble("bourate_addstbhd"));
                                stb.setLco_price(basicObj.getDouble("lco_price"));
                                stb.setBouquet_name(basicObj.getString("bouquet_name"));
                                stb.setBourate_mainstbsd(basicObj.getDouble("bourate_addstbsd"));
                                stb.setDiscount(basicObj.getDouble("discount"));
                                stbArrayList.add(stb);
                            }
                            Values.STB_ARRAY = stbArrayList;
                            Values.STB = stb;
                            subsNameTV.setText(Values.SUBSCRIBER.getSub_name());
                            subsCodeTV.setText(Values.SUBSCRIBER.getSub_code());
                            subsAddressTV.setText(Values.SUBSCRIBERDETAILS.getAddress1());
                            mobileTV.setText(Values.SUBSCRIBERDETAILS.getPhone1());
                            emailTV.setText(Values.SUBSCRIBERDETAILS.getEmail());
                            associateIdTV.setText(Values.SUBSCRIBERDETAILS.getAssociate_id());
                            subscriberStatusTV.setText(Values.SUBSCRIBERDETAILS.getStatus());
                            if(Values.SUBSCRIBERDETAILS.getStatus().equalsIgnoreCase("Active")){
                                subscriberStatusTV.setTextColor(getResources().getColor(R.color.green));
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        disconnectTL.setVisibility(View.VISIBLE);
                                        disconnectButton.setVisibility(View.VISIBLE);
                                        Values.SUBSCRIBER_SERVICE = "DIS";
                                    }
                                });
                            }else{
                                Values.SUBSCRIBER_SERVICE = "RECON";
                                reasonTR.setVisibility(View.GONE);
                                disconDateTR.setVisibility(View.GONE);
                                reconnectButton.setVisibility(View.VISIBLE);
                                subscriberStatusTV.setTextColor(getResources().getColor(R.color.red));
                            }


                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(SubscriberServiceActivity.this, android.R.layout.simple_spinner_item, Values.MAC_ID_LIST);
                            macIdSpinner.setAdapter(adapter);
                        }
                    }catch (Exception ex){
                        Log.e("token exception ", ex.toString());
                    }
                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SubscriberServiceActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
    private class FetchReasons extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Json.
         */
        String json = "";
        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();
        /**
         * The Progress dialog.
         */
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SubscriberServiceActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;

            try {
                url = new URL(Constants.REASONS_API+"?token="+ Values.TOKEN);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("Connection ",conn.toString());
//                OutputStream os = conn.getOutputStream();
//                os.write(jsonObject.toString().getBytes("UTF-8"));
//                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
/*                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");*/
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray dataArray;
            JSONObject lcoPriceInfo;
            try {
                status = json.getBoolean("success");

                if(status){
                    reasonsArrayList.clear();
                    dataArray = json.getJSONArray("data");
                    for(int i = 0; i < dataArray.length();i++){
                        final JSONObject obj = dataArray.getJSONObject(i);
                        Reasons reasons = new Reasons();
                        reasons.setId(obj.getInt("id"));
                        reasons.setReason(obj.getString("reason"));
                        reasonsArrayList.add(reasons);
                        reasonIdList.add(obj.getInt("id"));
                        reasonList.add(obj.getString("reason"));
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(SubscriberServiceActivity.this, android.R.layout.simple_spinner_item, reasonList);
                    reasonSpinner.setAdapter(adapter);
                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SubscriberServiceActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
    private class DisconnectRequest extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SubscriberServiceActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            final JSONObject userObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;
            String effectiveDate = params[0],reconnectionDate= params[1],macId= params[2];

            try {
                url = new URL(Constants.DISCONNECTION_REQUEST);
                Calendar c = Calendar.getInstance();
                int month = c.get(Calendar.MONTH) + 1;
                String date =  c.get(Calendar.DAY_OF_MONTH)+ "-" +month+"-" + c.get(Calendar.YEAR) ;
                try {
                    jsonObject.put("trans_date", date);
                    jsonObject.put("lng_service_id", 4);
                    jsonObject.put("effective_date", effectiveDate);
                    jsonObject.put("likely_recn_date", reconnectionDate);
                    jsonObject.put("mac_id", macId);
                    jsonObject.put("sub_id", Values.SUBSCRIBER.getSub_id());
                    jsonObject.put("reason_id", Values.REASON_ID);
                    userObject.put("name",Values.USER_NAME);
                } catch (JSONException je) {
                    Log.e("Json Exception ", je.toString());
                    Log.e("Url",url.toString());
                }
                Log.e("JSON SEND", jsonObject.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                conn.connect();
                Log.e("Connection ",conn.toString());
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String msg;
            try {
                status = json.getBoolean("success");
                if(status) {
                    try {
                        msg = json.getString("msg");
                        if(status){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(SubscriberServiceActivity.this, msg, Toast.LENGTH_SHORT).show();
                                    Intent intent =new Intent(SubscriberServiceActivity.this,SuccessActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            });

                        }else{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(SubscriberServiceActivity.this, msg, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    } catch (Exception ex) {
                        Log.e("Exception ",ex.toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
    private class ReconnectRequest extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SubscriberServiceActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            final JSONObject userObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;
            String reconnectionDate= params[0],macId= params[1];

            try {
                url = new URL(Constants.RECONNECTION_REQUEST);
                Calendar c = Calendar.getInstance();
                int month = c.get(Calendar.MONTH) + 1;
                String date =  c.get(Calendar.DAY_OF_MONTH)+ "-" +month+"-" + c.get(Calendar.YEAR) ;
                try {
                    jsonObject.put("trans_date", date);
                    jsonObject.put("lng_service_id", 5);
                    jsonObject.put("effective_date", reconnectionDate);
                    jsonObject.put("likely_recn_date", "");
                    jsonObject.put("mac_id", macId);
                    jsonObject.put("sub_id", Values.SUBSCRIBER.getSub_id());
                    userObject.put("name",Values.USER_NAME);
                } catch (JSONException je) {
                    Log.e("Json Exception ", je.toString());
                    Log.e("Url",url.toString());
                }
                Log.e("JSON SEND", jsonObject.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                conn.connect();
                Log.e("Connection ",conn.toString());
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String msg;
            try {
                status = json.getBoolean("success");
                if(status) {
                    try {
                        msg = json.getString("msg");
                        if(status){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(SubscriberServiceActivity.this, msg, Toast.LENGTH_SHORT).show();
                                    Intent intent =new Intent(SubscriberServiceActivity.this,SuccessActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            });

                        }else{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(SubscriberServiceActivity.this, msg, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    } catch (Exception ex) {
                        Log.e("Exception ",ex.toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }

}
