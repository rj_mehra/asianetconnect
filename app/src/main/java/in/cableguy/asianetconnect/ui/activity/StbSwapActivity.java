package in.cableguy.asianetconnect.ui.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.helper.Constants;
import in.cableguy.asianetconnect.helper.Values;

public class StbSwapActivity extends AppCompatActivity {

    private TextView subsNameTV, subsAddressTV, cidTV,associateIdTV,actSubsNameTV,actSubsAddressTV,actCidTV,actAssociateIdTV;
    private EditText actualMacIdTV;
    private Spinner macIdSpinner;
    private Button submitMacIdButton,swapButton;
    private TableLayout actualMacTL;
    private int toSubId, fromSubId;
    private String SELECTED_MAC_ID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stb_swap);
        getSupportActionBar().setTitle("STB Swap");
        subsNameTV = (TextView)findViewById(R.id.subsNameTV);
        subsAddressTV = (TextView)findViewById(R.id.subsAddressTV);
        cidTV = (TextView)findViewById(R.id.cidTV);
        associateIdTV = (TextView)findViewById(R.id.associateIdTV);
        actSubsNameTV = (TextView)findViewById(R.id.actSubsNameTV);
        actSubsAddressTV = (TextView)findViewById(R.id.actSubsAddressTV);
        actCidTV = (TextView)findViewById(R.id.actCidTV);
        actAssociateIdTV = (TextView)findViewById(R.id.actAssociateIdTV);
        actualMacIdTV = (EditText) findViewById(R.id.actualMacIdTV);
        macIdSpinner = (Spinner)findViewById(R.id.macIdSpinner);
        submitMacIdButton = (Button)findViewById(R.id.submitMacIdButton);
        actualMacTL = (TableLayout)findViewById(R.id.actualMacTL);
        swapButton = (Button)findViewById(R.id.swapButton);
        new GetStbSwapDetails().execute();
        submitMacIdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actualMacTL.setVisibility(View.VISIBLE);
                new GetActualStbDetails().execute(actualMacIdTV.getText().toString());
            }
        });

        macIdSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_MAC_ID = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        swapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new StbSwapRequest().execute(toSubId+"",actualMacIdTV.getText().toString(),SELECTED_MAC_ID,fromSubId+"",associateIdTV.getText().toString(),
                        cidTV.getText().toString(),actAssociateIdTV.getText().toString(),actCidTV.getText().toString());
            }
        });
    }


    private class GetStbSwapDetails extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(StbSwapActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;

            try {
                url = new URL(Constants.SUBS_SWAP_SEARCH+"ac_no="+Values.SEARCH_TERM+"&token="+Values.TOKEN);
                //Log.e("JSON SEND", jsonObject.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("Connection ",conn.toString());
/*
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());
*/

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray stbCodeArray;
            JSONObject data, subs, subscriber_details;

            try {
                status = json.getBoolean("success");
                if(status){
                    data = json.getJSONObject("data");
                    subs = data.getJSONObject("subscriber");
                    subscriber_details = data.getJSONObject("subscriber_details");
                    stbCodeArray = data.getJSONArray("stbs");
                    subsNameTV.setText(subs.getString("sub_name"));
                    subsAddressTV.setText(subscriber_details.getString("address1"));
                    cidTV.setText(subscriber_details.getString("center_id"));
                    associateIdTV.setText(subscriber_details.getString("associate_id"));
                    fromSubId = subs.getInt("sub_id");
                    ArrayList<String> stbCodeList= new ArrayList<String>();
                    for(int i = 0; i < stbCodeArray.length();i++){
                        final JSONObject obj = stbCodeArray.getJSONObject(i);
                        stbCodeList.add(obj.getString("stb_code"));
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(StbSwapActivity.this, android.R.layout.simple_spinner_item, stbCodeList);
                    macIdSpinner.setAdapter(adapter);
                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(StbSwapActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
    private class GetActualStbDetails extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(StbSwapActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;
            String actualMac = params[0];

            try {
                url = new URL(Constants.STB_SWAP_FETCH_BY_MAC+"mac_id="+actualMac+"&token="+Values.TOKEN);
                //Log.e("JSON SEND", jsonObject.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("Connection ",conn.toString());
/*
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());
*/

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String msg;
            JSONObject data;

            try {
                status = json.getBoolean("success");
                if(status){
                    data = json.getJSONObject("subscriber");
                    actSubsNameTV.setText(data.getString("name"));
                    actSubsAddressTV.setText(data.getString("address"));
                    actCidTV.setText(data.getString("center_id"));
                    actAssociateIdTV.setText(data.getString("associate_id"));
                    toSubId = data.getInt("sub_id");
                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(StbSwapActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }

    private class StbSwapRequest extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(StbSwapActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            final JSONObject userObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;
            String to_sub_id = params[0];
            String physical_mac_id = params[1];
            String sys_mac_id = params[2];
            String from_sub_id = params[3];
            String from_associate_id = params[4];
            String from_center_id = params[5];
            String to_associate_id = params[6];
            String to_center_id = params[7];


            try {
                url = new URL(Constants.STB_SWAP);
                try {
                    jsonObject.put("to_sub_id", to_sub_id);
                    jsonObject.put("physical_mac_id", physical_mac_id);
                    jsonObject.put("sys_mac_id", sys_mac_id);
                    jsonObject.put("from_sub_id", from_sub_id);
                    jsonObject.put("from_associate_id", from_associate_id);
                    jsonObject.put("from_center_id", from_center_id);
                    jsonObject.put("to_associate_id", to_associate_id);
                    jsonObject.put("to_center_id", to_center_id);
                    userObject.put("name",Values.USER_NAME);
                    jsonObject.put("user",userObject);
                    jsonObject.put("token",Values.TOKEN);
                } catch (JSONException je) {
                    Log.e("Json Exception ", je.toString());
                    Log.e("Url",url.toString());
                }
                Log.e("JSON SEND", jsonObject.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                conn.connect();
                Log.e("Connection ",conn.toString());
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray macIdArray;
            JSONObject subs;

            try {
                status = json.getBoolean("success");
                msg = json.getString("msg");
                if(status){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(StbSwapActivity.this, msg, Toast.LENGTH_SHORT).show();
                            Intent intent =new Intent(StbSwapActivity.this,SuccessActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    });

                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(StbSwapActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
}
