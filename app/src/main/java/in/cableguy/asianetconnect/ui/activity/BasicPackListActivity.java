package in.cableguy.asianetconnect.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.adapter.BasicPackListAdapter;
import in.cableguy.asianetconnect.helper.Constants;
import in.cableguy.asianetconnect.helper.Values;
import in.cableguy.asianetconnect.model.BasicPack;

public class BasicPackListActivity extends AppCompatActivity {
    Spinner durationOptionSpinner;
    RecyclerView basicPackListRV;
    EditText searchBar;
    Map<String, BasicPack> basicPackMap = new HashMap<>();
    ArrayList<BasicPack> basicPackList = new ArrayList<>();
    ArrayList<String> basicPackNames = new ArrayList<>();
    Activity activity;
    BasicPackListAdapter basicPackListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_pack_list);
        getSupportActionBar().setTitle("Basic Packs");
        durationOptionSpinner = (Spinner) findViewById(R.id.durationOptionSpinner);
        basicPackListRV = (RecyclerView) findViewById(R.id.basicPackListRV);
        searchBar = (EditText) findViewById(R.id.searchBar);
        activity = BasicPackListActivity.this;

        durationOptionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Values.DURATION = parent.getSelectedItem().toString();
                switch (parent.getSelectedItem().toString()) {
                    case "1 month":
                        Values.DURATION = "30";
                        break;
                    case "3 months":
                        Values.DURATION = "90";
                        break;
                    case "6 months":
                        Values.DURATION = "180";
                        break;
                    case "12 months":
                        Values.DURATION = "360";
                        break;
                    case "Others":
                        Values.DURATION = "Others";
                        break;
                }
                new BasicPlanList().execute(parent.getSelectedItem().toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                BasicPackListActivity.this.basicPackListAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private class BasicPlanList extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(BasicPackListActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;
            String duration = params[0];

            try {
                basicPackList.clear();
                basicPackMap.clear();
                basicPackNames.clear();
                url = new URL(Constants.BASIC_PACK_LIST + "?duration=" + Values.DURATION + "&cat=BOU" + "&sub_id=" + Values.SUBSCRIBER_ID + "&token=" + Values.TOKEN );
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("Connection ", conn.toString());
//                OutputStream os = conn.getOutputStream();
//                os.write(jsonObject.toString().getBytes("UTF-8"));
//                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
/*                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");*/
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token, msg;
            JSONArray bouquetArray;
            JSONObject lcoPriceInfo;

            try {
                status = json.getBoolean("success");
                if (status) {
                    basicPackList.clear();
                    bouquetArray = json.getJSONArray("bouArray");
                    for (int i = 0; i < bouquetArray.length(); i++) {
                        final JSONObject obj = bouquetArray.getJSONObject(i);
                        BasicPack basicPack = new BasicPack();
                        basicPack.setBouquet_id(obj.getInt("bouquet_id"));
                        basicPack.setBouquet_name(obj.getString("bouquet_name"));
                        basicPack.setBouquet_type(obj.getString("bouquet_type"));
                        basicPack.setBouquet_period(obj.getString("bouquet_period"));
                        /*lcoPriceInfo = obj.getJSONObject("lco_price_info");
                        alacarte.setTotal_addstbhd(lcoPriceInfo.getInt("total_addstbhd"));
                        alacarte.setTotal_addstbsd(lcoPriceInfo.getInt("total_addstbsd"));
                        alacarte.setTotal_mainstbhd(lcoPriceInfo.getInt("total_mainstbhd"));
                        alacarte.setTotal_mainstbsd(lcoPriceInfo.getInt("total_mainstbsd"));*/
                        basicPack.setBourate_mainstbhd(obj.getInt("bourate_mainstbhd"));
                        basicPack.setBourate_mainstbsd(obj.getInt("bourate_mainstbsd"));
                        basicPack.setBourate_addstbhd(obj.getInt("bourate_addstbhd"));
                        basicPack.setBourate_addstbsd(obj.getInt("bourate_addstbsd"));
                        //basicPack.setAuto_renew("N");
                        basicPack.setNo_of_months(Integer.parseInt(basicPack.getBouquet_period().replace(" Month","").trim()));
                        basicPackList.add(basicPack);
                        basicPackNames.add(basicPack.getBouquet_name());
                        basicPackMap.put(basicPack.getBouquet_name(), basicPack);
                    }
                    //Values.ALACARTE = alacarte;
                    basicPackListAdapter = new BasicPackListAdapter(basicPackNames, basicPackList, basicPackMap, activity);
                    basicPackListRV.setAdapter(basicPackListAdapter);
                    LinearLayoutManager layoutManager
                            = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
                    basicPackListRV.setLayoutManager(layoutManager);

                } else {
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(BasicPackListActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
}
