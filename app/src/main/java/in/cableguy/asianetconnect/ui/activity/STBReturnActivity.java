package in.cableguy.asianetconnect.ui.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.helper.Constants;
import in.cableguy.asianetconnect.helper.Values;
import in.cableguy.asianetconnect.model.Dealers;
import in.cableguy.asianetconnect.model.MacIds;

public class STBReturnActivity extends AppCompatActivity {
    private TextView subsNameTV, addressTV,subsCodeTV,associateIdTV,associateNameTV,mdlTV,cidTV,sidTV,subStatusTV,
            mob1TV,mob2TV;
    private EditText reasonED;
    private ArrayList<String> macArrayList;
    private ArrayList<String> dealersArrayList;
    private SearchableSpinner dealerIdSpinner,macIdSpinner;
    private Spinner stbConditionSpinner;
    private Button submitButton;
    private ArrayList<Dealers> dealersList;
    private int stbCondition;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stbreturn);
        getSupportActionBar().setTitle("STB Return");
        subsNameTV = (TextView)findViewById(R.id.subsNameTV);
        addressTV = (TextView)findViewById(R.id.addressTV);
        subsCodeTV = (TextView)findViewById(R.id.subsCodeTV);
        associateIdTV = (TextView)findViewById(R.id.associateIdTV);
        associateNameTV = (TextView)findViewById(R.id.associateNameTV);
        mdlTV = (TextView)findViewById(R.id.mdlTV);
        cidTV = (TextView)findViewById(R.id.cidTV);
        sidTV = (TextView)findViewById(R.id.sidTV);
        subStatusTV = (TextView)findViewById(R.id.subStatusTV);
        mob1TV = (TextView)findViewById(R.id.mob1TV);
        mob2TV = (TextView)findViewById(R.id.mob2TV);
        macIdSpinner = (SearchableSpinner)findViewById(R.id.macIdSpinner);
        stbConditionSpinner = (Spinner)findViewById(R.id.stbConditionSpinner);
        dealerIdSpinner = (SearchableSpinner)findViewById(R.id.dealerIdSpinner);
        reasonED = (EditText)findViewById(R.id.reasonED);
        submitButton = (Button)findViewById(R.id.submitButton);
        dealerIdSpinner.setTitle("Select Dealer");
        dealerIdSpinner.setPositiveButton("Select");
        new GetStbDetails().execute();
        dealerIdSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                final Dealers dealers = dealersList.get(position);
                Values.DEALER_ID = dealers.getDealer_id();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        stbConditionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String condition = adapterView.getSelectedItem().toString();
                switch (condition){
                    case "Normal":
                            stbCondition = 1;
                        break;
                    case "Upgrade":
                            stbCondition = 2;
                        break;
                    case "Faulty":
                            stbCondition = 3;
                        break;
                    default:
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new StbReturnRequest().execute(cidTV.getText().toString(),sidTV.getText().toString(),macIdSpinner.getSelectedItem().toString());
            }
        });
/*
        {"center_id":"AA01","sub_id":"10042000",
        "mac_id":"38C85C715D90","dealer_id":"50440","stb_condition":"1","user":{"name":"Niranjanan.V"}}*/
    }

    private class GetStbDetails extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(STBReturnActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;

            try {
                url = new URL(Constants.STB_DETAILS_RETURN);
                try {
                    jsonObject.put("sub_code", Values.SEARCH_TERM);
                    jsonObject.put("token", Values.TOKEN);
                } catch (JSONException je) {
                    Log.e("Json Exception ", je.toString());
                    Log.e("Url",url.toString());
                }
                Log.e("JSON SEND", jsonObject.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                conn.connect();
                Log.e("Connection ",conn.toString());
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray macIdArray;
            JSONObject subs;

            try {
                status = json.getBoolean("success");
                if(status){
                    subs = json.getJSONObject("subscriber");
                    macIdArray = subs.getJSONArray("macIds");

                    ArrayList<MacIds> macIdsArrayList = new ArrayList<MacIds>();
                    macArrayList = new ArrayList<String>();
                    for(int i = 0; i < macIdArray.length();i++){
                        MacIds macIds = new MacIds();
                        final JSONObject obj = macIdArray.getJSONObject(i);
                        macIds.setMacId(obj.getString("macId"));
                        macIds.setStbStatus(obj.getString("stbStatus"));
                        macArrayList.add(obj.getString("macId"));
                        macIdsArrayList.add(macIds);
                    }
                    Values.MACIDS_ARRAY = macIdsArrayList;
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(STBReturnActivity.this, android.R.layout.simple_spinner_item, macArrayList);
                    macIdSpinner.setAdapter(adapter);
                    subsNameTV.setText(subs.getString("subName"));
                    addressTV.setText(subs.getString("subAddress1")+" "+subs.getString("subAddress2")+" "+subs.getString("subAddress3"));
                    subsCodeTV.setText(subs.getString("subCode"));
                    associateIdTV.setText(subs.getString("associateId"));
                    associateNameTV.setText(subs.getString("associateName"));
                    mdlTV.setText(subs.getString("mdl"));
                    cidTV.setText(subs.getString("cid"));
                    sidTV.setText(subs.getString("sid"));
                    subStatusTV.setText(subs.getString("subStatus"));
                    if(subs.getString("subStatus").equalsIgnoreCase("inactive")){
                        subStatusTV.setTextColor(getResources().getColor(R.color.red));
                    }else{
                        subStatusTV.setTextColor(getResources().getColor(R.color.green));
                    }
                    mob1TV.setText(subs.getString("mobile1"));
                    mob2TV.setText(subs.getString("mobile2"));
                    new GetDealers().execute();
                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(STBReturnActivity.this, msg, Toast.LENGTH_SHORT).show();
                            submitButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Toast.makeText(STBReturnActivity.this, msg, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });

                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
    private class GetDealers extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(STBReturnActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;

            try {
                url = new URL(Constants.FETCH_DEALERS+cidTV.getText().toString()+"?token="+Values.TOKEN);
                Log.e("JSON SEND", jsonObject.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("Connection ",conn.toString());
/*
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());
*/

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray dealerArray;
            JSONObject subs;

            try {
                status = json.getBoolean("success");
                if(status){
                    dealerArray = json.getJSONArray("data");
                    dealersList = new ArrayList<Dealers>();
                    dealersArrayList = new ArrayList<String>();
                    for(int i = 0; i < dealerArray.length();i++){
                        Dealers dealers = new Dealers();
                        final JSONObject obj = dealerArray.getJSONObject(i);
                        dealers.setDealer_id(obj.getInt("dealer_id"));
                        dealers.setDealer_name(obj.getString("dealer_name"));
                        dealersArrayList.add(obj.getString("dealer_name"));
                        dealersList.add(dealers);
                    }
                    Values.DEALERS_ARRAY = dealersList;
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(STBReturnActivity.this, android.R.layout.simple_spinner_item, dealersArrayList);
                    dealerIdSpinner.setAdapter(adapter);

                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(STBReturnActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
    private class StbReturnRequest extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(STBReturnActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            final JSONObject userObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;
            String center_id = params[0];
            String sub_id = params[1];
            String mac_id = params[2];

            try {
                url = new URL(Constants.STB_RETURN);
                try {
                    jsonObject.put("center_id", center_id);
                    jsonObject.put("sub_id", sub_id);
                    jsonObject.put("mac_id", mac_id);
                    jsonObject.put("dealer_id", Values.DEALER_ID);
                    jsonObject.put("stb_condition", stbCondition);
                    userObject.put("name",Values.USER_NAME);
                    jsonObject.put("user",userObject);
                } catch (JSONException je) {
                    Log.e("Json Exception ", je.toString());
                    Log.e("Url",url.toString());
                }
                Log.e("JSON SEND", jsonObject.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                conn.connect();
                Log.e("Connection ",conn.toString());
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray macIdArray;
            JSONObject subs;

            try {
                status = json.getBoolean("success");
                msg = json.getString("msg");
                if(status){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(STBReturnActivity.this, msg, Toast.LENGTH_SHORT).show();
                            Intent intent =new Intent(STBReturnActivity.this,SuccessActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    });

                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(STBReturnActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
}
