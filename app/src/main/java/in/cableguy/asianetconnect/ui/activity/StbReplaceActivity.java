package in.cableguy.asianetconnect.ui.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import in.cableguy.asianetconnect.R;
import in.cableguy.asianetconnect.helper.Constants;
import in.cableguy.asianetconnect.helper.Values;
import in.cableguy.asianetconnect.model.DealerMacModel;
import in.cableguy.asianetconnect.model.Dealers;
import in.cableguy.asianetconnect.model.MacIds;

public class StbReplaceActivity extends AppCompatActivity {
    private TextView subsNameTV,subsAddressTV,subsCodeTV,subsBalanceTV,subsSchemeTV,cidTV,macIdTV;
    private Spinner macIdSpinner,returnSpinner,reasonRepSpinner;
    private SearchableSpinner dealerSpinner;
    private EditText searchED;
    private Button searchButton, submitButton;
    private ArrayList<String> dealersArrayList=new ArrayList<>();
    private ArrayList<Dealers> dealersList=new ArrayList<>();
    private ArrayList<Integer> dealerIdList=new ArrayList<>();
    private ArrayList<DealerMacModel> dealerMacModelsList=new ArrayList<>();

    private ArrayList<String> macArrayList=new ArrayList<>();

    private int reasonRep, returnFlag;

    private String searchTxt;
    private String SUBS_ID ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stb_replace);
        getSupportActionBar().setTitle("STB Replace");
        subsNameTV = (TextView)findViewById(R.id.subsNameTV);
        subsAddressTV = (TextView)findViewById(R.id.subsAddressTV);
        subsCodeTV = (TextView)findViewById(R.id.subsCodeTV);
        subsBalanceTV = (TextView)findViewById(R.id.subsBalanceTV);
        subsSchemeTV = (TextView)findViewById(R.id.subsSchemeTV);
        cidTV = (TextView)findViewById(R.id.cidTV);
        macIdTV = (TextView)findViewById(R.id.macIdTV);
        dealerSpinner = (SearchableSpinner)findViewById(R.id.dealerSpinner);
        macIdSpinner = (Spinner)findViewById(R.id.macIdSpinner);
        returnSpinner = (Spinner)findViewById(R.id.returnSpinner);
        reasonRepSpinner = (Spinner)findViewById(R.id.reasonRepSpinner);
        searchButton = (Button)findViewById(R.id.searchButton);
        submitButton = (Button)findViewById(R.id.submitButton);
        searchED = (EditText)findViewById(R.id.searchED);
        new GetSTBReplacementDetails().execute();
        returnSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String flag = adapterView.getSelectedItem().toString();
                switch(flag){
                    case "Yes":
                        returnFlag = 1;
                        break;
                    case "No":
                        returnFlag = 0;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchTxt = searchED.getText().toString();
                if(searchTxt.equalsIgnoreCase("")){
                    Toast.makeText(StbReplaceActivity.this,"Invalid Search Text",Toast.LENGTH_SHORT).show();
                }else{
                    new GetDealersByMACSearch().execute();
                }
            }
        });

        dealerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                /*Check if dealer Mac Model List is populated or not*/
                if(dealerMacModelsList.isEmpty()){
                    final Dealers dealers = dealersList.get(i);
                    Values.DEALER_ID = dealers.getDealer_id();
                    new GetFreeSTBs().execute();
                }else{
                    final DealerMacModel  dealer = dealerMacModelsList.get(i);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(StbReplaceActivity.this, android.R.layout.simple_spinner_item, dealer.getMacIdArrayList());
                    macIdSpinner.setAdapter(adapter);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        reasonRepSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String condition = adapterView.getSelectedItem().toString();
                switch (condition){
                    case "Normal":
                        reasonRep = 1;
                        break;
                    case "Upgrade":
                        reasonRep = 2;
                        break;
                    case "Faulty":
                        reasonRep = 3;
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new StbReplaceRequest().execute();
            }
        });
    }

    private class GetDealers extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(StbReplaceActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;

            try {
                url = new URL(Constants.FETCH_DEALERS+cidTV.getText().toString()+"?token="+Values.TOKEN);
                Log.e("JSON SEND", jsonObject.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("Connection ",conn.toString());
/*
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());
*/

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray dealerArray;
            JSONObject subs;

            try {
                status = json.getBoolean("success");
                if(status){
                    dealerArray = json.getJSONArray("data");
                    dealersList = new ArrayList<Dealers>();
                    dealersArrayList = new ArrayList<String>();
                    for(int i = 0; i < dealerArray.length();i++){
                        final JSONObject obj = dealerArray.getJSONObject(i);
                        Dealers dealers = new Dealers();
                        dealers.setDealer_id(obj.getInt("dealer_id"));
                        dealers.setDealer_name(obj.getString("dealer_name"));
                        dealersArrayList.add(obj.getString("dealer_name"));
                        dealersList.add(dealers);
                    }
                    Values.DEALERS_ARRAY = dealersList;
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(StbReplaceActivity.this, android.R.layout.simple_spinner_item, dealersArrayList);
                    dealerSpinner.setAdapter(adapter);

                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(StbReplaceActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
    private class GetFreeSTBs extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(StbReplaceActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;

            try {
                url = new URL(Constants.FETCH_FREE_STB_FOR_DEALER+Values.DEALER_ID+"?token="+Values.TOKEN);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("Connection ",conn.toString());
/*
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());
*/

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {
                Log.e("Ex",e.toString());
            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray dealerArray,macIdArray;
            JSONObject subs;

            try {
                status = json.getBoolean("success");
                if(status){
                    macIdArray = json.getJSONArray("data");
                    ArrayList<MacIds> macIdsArrayList = new ArrayList<MacIds>();
                    macArrayList = new ArrayList<String>();
                    for(int i = 0; i < macIdArray.length();i++){
                        MacIds macIds = new MacIds();
                        final JSONObject obj = macIdArray.getJSONObject(i);
                        macIds.setMacId(obj.getString("mac_id"));
                        macIds.setDelimitedMacId(obj.getString("delimited_mac_id"));
                        //macIds.setStbStatus(obj.getString("stbStatus"));
                        macArrayList.add(obj.getString("mac_id"));
                        macIdsArrayList.add(macIds);
                    }
                    Values.MACIDS_ARRAY = macIdsArrayList;
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(StbReplaceActivity.this, android.R.layout.simple_spinner_item, macArrayList);
                    macIdSpinner.setAdapter(adapter);


                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(StbReplaceActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
    private class GetSTBReplacementDetails extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(StbReplaceActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;

            try {
                String strURL = Constants.STB_DETAILS_REPLACEMENT+"?token="+Values.TOKEN;
                if(Values.SEARCH_PARAM.equalsIgnoreCase("SUB_CODE")){
                    strURL +="&ac_no="+Values.SEARCH_TERM;
                }else{
                    strURL +="&stb_no="+Values.SEARCH_TERM;
                }
                url = new URL(strURL);
                /*try {
                    jsonObject.put("sub_code", Values.SEARCH_TERM);
                    jsonObject.put("token", Values.TOKEN);
                } catch (JSONException je) {
                    Log.e("Json Exception ", je.toString());
                    Log.e("Url",url.toString());
                }*/
                Log.e("JSON SEND", jsonObject.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("Connection ",conn.toString());
                /*OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());*/

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray macIdArray;
            JSONObject subs, subscriberDetails,data;

            try {
                status = json.getBoolean("success");
                if(status){
                    data = json.getJSONObject("data");
                    subs = data.getJSONObject("subscriber");
                    subscriberDetails = data.getJSONObject("subscriber_details");
                    SUBS_ID = subs.getString("sub_id");
                    subsNameTV.setText(subs.getString("sub_name"));
                    subsAddressTV.setText(subscriberDetails.getString("address1"));
                    subsCodeTV.setText(subs.getString("sub_code"));
                    subsBalanceTV.setText(subscriberDetails.getString("balance"));
                    subsSchemeTV.setText(subscriberDetails.getString("scheme"));
                    cidTV.setText(subscriberDetails.getString("center_id"));
                    new GetDealers().execute();
                    macIdTV.setText(subscriberDetails.getString("mac_id"));
                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(StbReplaceActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }
    private class GetDealersByMACSearch extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(StbReplaceActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;

            try {
                url = new URL(Constants.FETCH_DEALERS_BY_MAC+"?token="+Values.TOKEN);
                Log.e("JSON SEND", jsonObject.toString());
                jsonObject.put("search_txt",searchTxt);
                jsonObject.put("center_id",cidTV.getText().toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                conn.connect();
                Log.e("Connection ",conn.toString());
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray dealerArray;
            JSONObject subs;
            JSONArray macArray;

            try {
                status = json.getBoolean("success");
                if(status){
                    dealerArray = json.getJSONArray("data");

                    dealerMacModelsList = new ArrayList<DealerMacModel>();
                    dealersArrayList = new ArrayList<String>();
                    for(int i = 0; i < dealerArray.length();i++){
                        DealerMacModel dealers = new DealerMacModel();
                        final JSONObject obj = dealerArray.getJSONObject(i);
                        dealers.setDealer_id(obj.getInt("dealer_id"));
                        dealers.setDealer_name(obj.getString("dealer_name"));
                        JSONArray macIdArray = obj.getJSONArray("macIds");
                        ArrayList<MacIds> macIds = new ArrayList<>();
                        ArrayList<String> macIdsArrayList = new ArrayList<>();

                        for(int j=0;j<macIdArray.length();j++) {
                            MacIds macId = new MacIds();
                            final JSONObject macIdObj = macIdArray.getJSONObject(j);
                            macId.setMacId(macIdObj.getString("mac_id"));
                            macId.setDelimitedMacId(macIdObj.getString("delimited_mac_id"));
                            macIdsArrayList.add(macId.getMacId());
                            macIds.add(macId);
                        }
                        dealers.setMacIds(macIds);
                        dealers.setMacIdArrayList(macIdsArrayList);
                        dealersArrayList.add(obj.getString("dealer_name"));
                        dealerMacModelsList.add(dealers);
                    }
                    //Values.DEALERS_ARRAY = dealersList;
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(StbReplaceActivity.this, android.R.layout.simple_spinner_item, dealersArrayList);
                    dealerSpinner.setAdapter(adapter);

                }else{
                    msg = json.getString("msg");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(StbReplaceActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }

    private class StbReplaceRequest extends AsyncTask<String, Void, JSONObject> {
        InputStream is = null;
        String json = "";
        StringBuilder sb = new StringBuilder();
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(StbReplaceActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject respJSONObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();
            final JSONObject userObject = new JSONObject();
            InputStream is = null;
            StringBuilder sb = new StringBuilder();
            String json = "";
            URL url;

            try {
                url = new URL(Constants.STB_REPLACEMENT_REQUEST+"?token="+Values.TOKEN);
                try {
                    jsonObject.put("sub_id", SUBS_ID);
                    jsonObject.put("old_mac_id", macIdTV.getText().toString());
                    jsonObject.put("new_mac_id", macIdSpinner.getSelectedItem().toString());
                    jsonObject.put("dealer_id", Values.DEALER_ID+"");
                    jsonObject.put("returned_flag", returnFlag);
                    jsonObject.put("reason", "");
                    jsonObject.put("stb_condition", reasonRep);
                    jsonObject.put("remark", "");
                    userObject.put("name",Values.USER_NAME);
                    jsonObject.put("user",userObject);
                } catch (JSONException je) {
                    Log.e("Json Exception ", je.toString());
                    Log.e("Url",url.toString());
                }
                Log.e("JSON SEND", jsonObject.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                conn.connect();
                Log.e("Connection ",conn.toString());
                OutputStream os = conn.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                Log.e("OS : ",os.toString());

                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode : ", "" + statuscode);
                Log.e("response : ", conn.getResponseMessage());

                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                json = sb.toString();
                json = json.replace("\\", "");
                json = json.replace("\"{", "{");
                json = json.replace("}\"", "}");
                json = sb.toString();
                Log.e("JValue : ", "value : " + json);
                //respJSONObject=new JSONObject();
                try {
                    respJSONObject = new JSONObject(json);
                } catch (JSONException je) {
                    Log.e("JSONEX : ", je.toString());
                }
            } catch (Exception e) {

            }
            return respJSONObject;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        protected void onPostExecute(JSONObject json) {
            Boolean status;
            final String token,msg;
            JSONArray macIdArray;
            JSONObject subs;

            try {
                status = json.getBoolean("success");
                msg = json.getString("msg");
                if(status){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(StbReplaceActivity.this, msg, Toast.LENGTH_SHORT).show();
                            Intent intent =new Intent(StbReplaceActivity.this,SuccessActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    });

                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(StbReplaceActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }

}
