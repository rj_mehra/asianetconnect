package in.cableguy.asianetconnect.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.cableguy.asianetconnect.model.Alacarte;
import in.cableguy.asianetconnect.model.Dealers;
import in.cableguy.asianetconnect.model.MacIds;
import in.cableguy.asianetconnect.model.Menu;
import in.cableguy.asianetconnect.model.Stb;
import in.cableguy.asianetconnect.model.Subscriber;
import in.cableguy.asianetconnect.model.SubscriberDetails;

/**
 * Created by rajme on 8/17/2017.
 */

public class Values {

    public static String CURRENT_VERSION = "";
    public static Boolean LOC_PERM_GRANT = false;
    public static Boolean READ_PERM_GRANT = false;
    public static String TOKEN = "";
    public static String USER_NAME = "";
    public static Subscriber SUBSCRIBER;
    public static Stb STB;
    public static SubscriberDetails SUBSCRIBERDETAILS;
    public static int SUBSCRIBER_ID;
    public static String SUBSCRIBER_CODE="";
    public static String SUBSCRIBER_ASSOCIATE_ID = "";
    public static String SUBSCRIBER_CENTER_ID = "";
    public static ArrayList<Stb> STB_ARRAY;
    public static int POSITION=0;
    public static String SELECTED_MAC_ID = "";
    public static Alacarte ALACARTE;
    public static String STB_TYPE = "";
    public static String STB_CAT = "";
    public static String GENRE_ID = "";
    public static String SERVICE_ID = "";
    public static String DURATION = "";
    public static ArrayList<Alacarte> SELECTED_ALA_IDS = new ArrayList<Alacarte>();
    public static String OLD_BOUQUET_ID;
    public static ArrayList<MacIds> MACIDS_ARRAY;
    public static ArrayList<Dealers> DEALERS_ARRAY;
    public static int DEALER_ID = 0;
    public static String SEARCH_TERM = "";
    public static String CUSTOM_SEARCH_TYPE = "";
    public static String SEARCH_PARAM ="SUB_CODE";
    public static String UNAUTHORIZED_ACCESS="Unauthorized access. Kindly,contact administrator";

    public static Map<String,Menu> menuNameMap = new HashMap<>();
    public static Map<String,ArrayList<String>> childMenuNameMap = new HashMap<>();
    public static ArrayList<Menu> menuArrayList = new ArrayList<>();
    public static ArrayList<String> menuAccessList = new ArrayList<>();

    public static String SUB_MENU_PACK_MANAGEMENT = "/pack_management";
    public static String SUB_MENU_SUBSCRIBER_SERVICE = "/subscriber_action";
    public static String SUB_MENU_STB_REPLACE = "/stb_replace";
    public static String SUB_MENU_STB_SWAP = "/stb_swap";
    public static String SUB_MENU_STB_RETURN = "/stb_return";

    public static String SELECTED_SUB_MENU = "";

    public static String SUB_MENU_SUBS_SERV_DISCONNECTION = "disconnection";

    public static String SUB_MENU_SUBS_SERV_RECONNECTION = "reconnection";

    public static ArrayList<String> MAC_ID_LIST = new ArrayList<>();

    public static String SUBSCRIBER_SERVICE = "";

    public static int REASON_ID = 0;


}
