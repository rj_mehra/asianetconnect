package in.cableguy.asianetconnect.helper;

/**
 * Created by rajme on 8/17/2017.
 */

public class Constants {
    public static String API_URL = "http://124.153.73.30:16000/";
    public static String LOGIN_API = API_URL+"authenticate";
    public static String CUSTOMER_SEARCH = API_URL+"subscriber/search";
    public static String SUBSCRIBED_ALACARTES = API_URL+"channel/fetchSubscribedSchemes/";
    public static String ALA_CARTE_ACTION = API_URL+"subscriber/deact/channel";
    public static String ALACARTE_LIST = API_URL+"bouquet/search";
    public static String STB_ACTION = API_URL+"subscriber/stb/action";
    public static String CHANNEL_GENRES = API_URL+"channel/fetch/genres/";
    public static String FETCH_SERVICES = API_URL+"channel/fetch/services/";
    public static String ADD_ALACARTE = API_URL+"subscriber/assign/alacarte/";

    public static String BASIC_PACK_LIST = API_URL+"bouquet/search";
    public static String BASIC_PACK_CHANGE = API_URL+"subscriber/change/bouquet";
    public static String STB_DETAILS_RETURN = API_URL+"subscriber/stb/return/searchBySubCode";
    public static String STB_DETAILS_REPLACEMENT = API_URL+"reports/subscriber/info";
    public static String FETCH_FREE_STB_FOR_DEALER=API_URL+"stb/fetch/freeSTB/";
    public static String FETCH_DEALERS = API_URL+"stb/fetch/dealers/";
    public static String STB_RETURN = API_URL+"subscriber/stb/return";
    public static String STB_SWAP = API_URL+"stb/swap";
    public static String FETCH_DEALERS_BY_MAC = API_URL+"stb/fetch/dealersByMac";
    public static String FETCH_STB_SWAP_MAC = "/subscriber/fetch/MacIds";
    public static String STB_SWAP_FETCH_BY_MAC = API_URL+"subscriber/fetchByMacId?";
    public static String SUBS_SWAP_SEARCH = API_URL+"subscriber/fetch/MacIds?";
    public static String STB_REPLACEMENT_REQUEST = API_URL+"stb/replacement";
    public static String DISCONNECTION_REQUEST = API_URL+"subscriber/mac/dcrrequest";
    public static String RECONNECTION_REQUEST = API_URL+"subscriber/mac/dcrrequest";
    public static String REASONS_API = API_URL + "stb/disconnect/reasons";
}
