package in.cableguy.asianetconnect.model;

/**
 * Created by rajme on 8/18/2017.
 */

public class Subscriber {

    private String sub_name,sub_code;
    private int  sub_id;

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public String getSub_code() {
        return sub_code;
    }

    public void setSub_code(String sub_code) {
        this.sub_code = sub_code;
    }

    public int getSub_id() {
        return sub_id;
    }

    public void setSub_id(int sub_id) {
        this.sub_id = sub_id;
    }
}
