package in.cableguy.asianetconnect.model;

/**
 * Created by Raj on 31/08/2017.
 */

/**
 * bouquet_id: data.lngid,
 bouquet_name: data.schemeName,
 bouquet_type: 'SD',
 bouquet_period: '1 Month',
 lco_price_info: {
 "total_addstbhd": 0,
 "total_addstbsd": 0,
 "total_mainstbhd": 0,
 "total_mainstbsd": 0
 },
 "bourate_mainstbhd": data.rateMain,
 "bourate_mainstbsd": data.rateMain,
 "bourate_addstbhd": data.rateAdditional,
 "bourate_addstbsd": data.rateAdditional,
 */
public class BasicPack {
    String bouquet_name,bouquet_type,start_date,end_date,stb_code, bouquet_period, auto_renew;
    int bouquet_id;
    int total_addstbhd,total_addstbsd,total_mainstbhd, no_of_months,
    total_mainstbsd,bourate_mainstbhd,bourate_mainstbsd,bourate_addstbhd,bourate_addstbsd;

    public String getBouquet_name() {
        return bouquet_name;
    }

    public void setBouquet_name(String bouquet_name) {
        this.bouquet_name = bouquet_name;
    }

    public String getBouquet_type() {
        return bouquet_type;
    }

    public void setBouquet_type(String bouquet_type) {
        this.bouquet_type = bouquet_type;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStb_code() {
        return stb_code;
    }

    public void setStb_code(String stb_code) {
        this.stb_code = stb_code;
    }

    public String getBouquet_period() {
        return bouquet_period;
    }

    public void setBouquet_period(String bouquet_period) {
        this.bouquet_period = bouquet_period;
    }

    public String getAuto_renew() {
        return auto_renew;
    }

    public void setAuto_renew(String auto_renew) {
        this.auto_renew = auto_renew;
    }

    public int getBouquet_id() {
        return bouquet_id;
    }

    public void setBouquet_id(int bouquet_id) {
        this.bouquet_id = bouquet_id;
    }

    public int getTotal_addstbhd() {
        return total_addstbhd;
    }

    public void setTotal_addstbhd(int total_addstbhd) {
        this.total_addstbhd = total_addstbhd;
    }

    public int getTotal_addstbsd() {
        return total_addstbsd;
    }

    public void setTotal_addstbsd(int total_addstbsd) {
        this.total_addstbsd = total_addstbsd;
    }

    public int getTotal_mainstbhd() {
        return total_mainstbhd;
    }

    public void setTotal_mainstbhd(int total_mainstbhd) {
        this.total_mainstbhd = total_mainstbhd;
    }

    public int getNo_of_months() {
        return no_of_months;
    }

    public void setNo_of_months(int no_of_months) {
        this.no_of_months = no_of_months;
    }

    public int getTotal_mainstbsd() {
        return total_mainstbsd;
    }

    public void setTotal_mainstbsd(int total_mainstbsd) {
        this.total_mainstbsd = total_mainstbsd;
    }

    public int getBourate_mainstbhd() {
        return bourate_mainstbhd;
    }

    public void setBourate_mainstbhd(int bourate_mainstbhd) {
        this.bourate_mainstbhd = bourate_mainstbhd;
    }

    public int getBourate_mainstbsd() {
        return bourate_mainstbsd;
    }

    public void setBourate_mainstbsd(int bourate_mainstbsd) {
        this.bourate_mainstbsd = bourate_mainstbsd;
    }

    public int getBourate_addstbhd() {
        return bourate_addstbhd;
    }

    public void setBourate_addstbhd(int bourate_addstbhd) {
        this.bourate_addstbhd = bourate_addstbhd;
    }

    public int getBourate_addstbsd() {
        return bourate_addstbsd;
    }

    public void setBourate_addstbsd(int bourate_addstbsd) {
        this.bourate_addstbsd = bourate_addstbsd;
    }
}
