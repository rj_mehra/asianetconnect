package in.cableguy.asianetconnect.model;

import java.util.ArrayList;

/**
 * Created by Parag on 11/9/2017.
 */

public class SubMenu {
    private int id;
    private String name;
    private String route;
    private String type;
    private ArrayList<SubMenu> childMenus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<SubMenu> getChildMenus() {
        return childMenus;
    }

    public void setChildMenus(ArrayList<SubMenu> childMenus) {
        this.childMenus = childMenus;
    }
}
