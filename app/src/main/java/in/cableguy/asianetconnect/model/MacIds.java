package in.cableguy.asianetconnect.model;

/**
 * Created by Raj on 31/10/2017.
 */

public class MacIds {
    private String macId, delimitedMacId, stbStatus;

    public String getMacId() {
        return macId;
    }

    public void setMacId(String macId) {
        this.macId = macId;
    }

    public String getStbStatus() {
        return stbStatus;
    }

    public void setStbStatus(String stbStatus) {
        this.stbStatus = stbStatus;
    }


    public String getDelimitedMacId() {
        return delimitedMacId;
    }

    public void setDelimitedMacId(String delimitedMacId) {
        this.delimitedMacId = delimitedMacId;
    }

    @Override
    public String toString() {
        return "MacIds{" +
                "macId='" + macId + '\'' +
                ", delimitedMacId='" + delimitedMacId + '\'' +
                ", stbStatus='" + stbStatus + '\'' +
                '}';
    }
}
