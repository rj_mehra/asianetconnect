package in.cableguy.asianetconnect.model;

import java.util.ArrayList;

/**
 * Created by Parag on 11/9/2017.
 */

public class Menu {
    private int id;
    private String name;
    private ArrayList<SubMenu> subMenus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<SubMenu> getSubMenus() {
        return subMenus;
    }

    public void setSubMenus(ArrayList<SubMenu> subMenus) {
        this.subMenus = subMenus;
    }
}
