package in.cableguy.asianetconnect.model;

/**
 * Created by Raj on 03/11/2017.
 */

public class Dealers {
    private int dealer_id;
    private String dealer_name;

    public int getDealer_id() {
        return dealer_id;
    }

    public void setDealer_id(int dealer_id) {
        this.dealer_id = dealer_id;
    }

    public String getDealer_name() {
        return dealer_name;
    }

    public void setDealer_name(String dealer_name) {
        this.dealer_name = dealer_name;
    }
}
