package in.cableguy.asianetconnect.model;

/**
 * Created by rajme on 8/18/2017.
 */

public class Stb {

    private String stb_code, smart_card_no, stb_status, stb_cat, bouquet_id, start_date, end_date,bouquet_name;
    private Double bourate_mainstbhd, lco_price,
            bourate_mainstbsd,discount;

    public String getBouquet_name() {
        return bouquet_name;
    }

    public void setBouquet_name(String bouquet_name) {
        this.bouquet_name = bouquet_name;
    }

    public Double getBourate_mainstbhd() {
        return bourate_mainstbhd;
    }

    public void setBourate_mainstbhd(Double bourate_mainstbhd) {
        this.bourate_mainstbhd = bourate_mainstbhd;
    }

    public Double getLco_price() {
        return lco_price;
    }

    public void setLco_price(Double lco_price) {
        this.lco_price = lco_price;
    }

    public Double getBourate_mainstbsd() {
        return bourate_mainstbsd;
    }

    public void setBourate_mainstbsd(Double bourate_mainstbsd) {
        this.bourate_mainstbsd = bourate_mainstbsd;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getStb_code() {
        return stb_code;
    }

    public void setStb_code(String stb_code) {
        this.stb_code = stb_code;
    }

    public String getSmart_card_no() {
        return smart_card_no;
    }

    public void setSmart_card_no(String smart_card_no) {
        this.smart_card_no = smart_card_no;
    }

    public String getStb_status() {
        return stb_status;
    }

    public void setStb_status(String stb_status) {
        this.stb_status = stb_status;
    }

    public String getStb_cat() {
        return stb_cat;
    }

    public void setStb_cat(String stb_cat) {
        this.stb_cat = stb_cat;
    }

    public String getBouquet_id() {
        return bouquet_id;
    }

    public void setBouquet_id(String bouquet_id) {
        this.bouquet_id = bouquet_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

}
