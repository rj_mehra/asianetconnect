package in.cableguy.asianetconnect.model;

import java.util.ArrayList;

/**
 * Created by Parag on 11/6/2017.
 */

public class DealerMacModel {
    private int dealer_id;
    private String dealer_name;
    private ArrayList<MacIds> MacIds = new ArrayList<>();
    private ArrayList<String> MacIdArrayList = new ArrayList<>();

    public int getDealer_id() {
        return dealer_id;
    }

    public void setDealer_id(int dealer_id) {
        this.dealer_id = dealer_id;
    }

    public String getDealer_name() {
        return dealer_name;
    }

    public void setDealer_name(String dealer_name) {
        this.dealer_name = dealer_name;
    }

    public ArrayList<in.cableguy.asianetconnect.model.MacIds> getMacIds() {
        return MacIds;
    }

    public void setMacIds(ArrayList<in.cableguy.asianetconnect.model.MacIds> macIds) {
        MacIds = macIds;
    }

    public ArrayList<String> getMacIdArrayList() {
        return MacIdArrayList;
    }

    public void setMacIdArrayList(ArrayList<String> macIdArrayList) {
        MacIdArrayList = macIdArrayList;
    }
}
