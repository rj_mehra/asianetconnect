package in.cableguy.asianetconnect.model;

/**
 * Created by rajme on 8/20/2017.
 */

public class Alacarte {

    String channel_name,channel_type,start_date,end_date,stb_code, total_period, auto_renew;
    int channel_id, channel_service_sch_id,total_addstbhd,total_addstbsd,total_mainstbhd, no_of_months,
            total_mainstbsd,chanrate_mainstbhd,chanrate_mainstbsd,chanrate_addstbhd,chanrate_addstbsd;

    private String autoRenewFlag="N"; //to be used during adding alacarte

    public String getAutoRenewFlag() {
        return autoRenewFlag;
    }

    public void setAutoRenewFlag(String autoRenewFlag) {
        this.autoRenewFlag = autoRenewFlag;
    }

    public int getNo_of_months() {
        return no_of_months;
    }

    public void setNo_of_months(int no_of_months) {
        this.no_of_months = no_of_months;
    }

    public String getAuto_renew() {
        return auto_renew;
    }

    public void setAuto_renew(String auto_renew) {
        this.auto_renew = auto_renew;
    }

    public String getTotal_period() {
        return total_period;
    }

    public void setTotal_period(String total_period) {
        this.total_period = total_period;
    }

    public String getChannel_name() {
        return channel_name;
    }

    public void setChannel_name(String channel_name) {
        this.channel_name = channel_name;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStb_code() {
        return stb_code;
    }

    public int getChanrate_mainstbhd() {
        return chanrate_mainstbhd;
    }

    public void setChanrate_mainstbhd(int chanrate_mainstbhd) {
        this.chanrate_mainstbhd = chanrate_mainstbhd;
    }

    public void setStb_code(String stb_code) {
        this.stb_code = stb_code;

    }



    public int getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(int channel_id) {
        this.channel_id = channel_id;
    }

    public int getChannel_service_sch_id() {
        return channel_service_sch_id;
    }

    public void setChannel_service_sch_id(int channel_service_sch_id) {
        this.channel_service_sch_id = channel_service_sch_id;
    }

    public int getTotal_addstbhd() {
        return total_addstbhd;
    }

    public void setTotal_addstbhd(int total_addstbhd) {
        this.total_addstbhd = total_addstbhd;
    }

    public int getTotal_addstbsd() {
        return total_addstbsd;
    }

    public void setTotal_addstbsd(int total_addstbsd) {
        this.total_addstbsd = total_addstbsd;
    }

    public int getTotal_mainstbhd() {
        return total_mainstbhd;
    }

    public void setTotal_mainstbhd(int total_mainstbhd) {
        this.total_mainstbhd = total_mainstbhd;
    }

    public int getTotal_mainstbsd() {
        return total_mainstbsd;
    }

    public void setTotal_mainstbsd(int total_mainstbsd) {
        this.total_mainstbsd = total_mainstbsd;
    }

    public int getChanrate_mainstbsd() {
        return chanrate_mainstbsd;
    }

    public void setChanrate_mainstbsd(int chanrate_mainstbsd) {
        this.chanrate_mainstbsd = chanrate_mainstbsd;
    }

    public int getChanrate_addstbhd() {
        return chanrate_addstbhd;
    }

    public void setChanrate_addstbhd(int chanrate_addstbhd) {
        this.chanrate_addstbhd = chanrate_addstbhd;
    }

    public int getChanrate_addstbsd() {
        return chanrate_addstbsd;
    }

    public void setChanrate_addstbsd(int chanrate_addstbsd) {
        this.chanrate_addstbsd = chanrate_addstbsd;
    }
}
